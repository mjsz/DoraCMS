'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
  },

  
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Web_Configs', [
      {createdAt: new Date(),updatedAt: new Date(),key:"_id",value:"HyH1BsG7W"},
      {createdAt: new Date(),updatedAt: new Date(),key:"siteKeywords",value:"前端开发俱乐部,前端俱乐部,DoraCMS,Nodejs内容管理系统, 前端开发, web前端, 前端开发工程师,前端资源, angularjs, 前端开发工具, nodejs ,boostrap"},
      {createdAt: new Date(),updatedAt: new Date(),key:"siteEmail",value:"522240095@qq.com"},
      {createdAt: new Date(),updatedAt: new Date(),key:"databackForderPath",value:"/home/database/doracms/"},
      {createdAt: new Date(),updatedAt: new Date(),key:"mongodbInstallPath",value:"/usr/local/mongodb/mongodb-linux-x86_64-ubuntu1604-4.0.0/bin/"},
      {createdAt: new Date(),updatedAt: new Date(),key:"registrationNo",value:"粤ICP备15038960号-2"},
      {createdAt: new Date(),updatedAt: new Date(),key:"siteDiscription",value:"前端开发俱乐部,分享前端知识,丰富前端技能。汇集国内专业的前端开发文档,为推动业内前端开发水平共同奋斗。DoraCMS,html,js,css,nodejs,前端开发,jquery,web前端, web前端开发, 前端开发工程师"},
      {createdAt: new Date(),updatedAt: new Date(),key:"siteDomain",value:"https://www.html-js.cn"},
      {createdAt: new Date(),updatedAt: new Date(),key:"siteName",value:"前端开发俱乐部"},
      {createdAt: new Date(),updatedAt: new Date(),key:"__v",value:0},
      {createdAt: new Date(),updatedAt: new Date(),key:"siteEmailPwd",value:"U2FsdGVkX19ipgNmDJ2FjX3r3KsGdutctMSnUxJ88UQCfNehBkGBiSKYVt8KogdV"},
      {createdAt: new Date(),updatedAt: new Date(),key:"siteEmailServer",value:"QQ"},
      {createdAt: new Date(),updatedAt: new Date(),key:"showImgCode",value:1},
      {createdAt: new Date(),updatedAt: new Date(),key:"shareArticlScore",value:1},
      {createdAt: new Date(),updatedAt: new Date(),key:"postMessageScore",value:1},
      {createdAt: new Date(),updatedAt: new Date(),key:"poseArticlScore",value:1},
      {createdAt: new Date(),updatedAt: new Date(),key:"siteAltKeywords",value:"前端开发俱乐部,前端俱乐部,DoraCMS,Nodejs内容管理系统, 前端开发, web前端, 前端开发工程师,前端资源, angularjs, 前端开发工具, nodejs ,boostrap"},
      {createdAt: new Date(),updatedAt: new Date(),key:"bakDataRate",value:"3"},
      {createdAt: new Date(),updatedAt: new Date(),key:"bakDatabyTime",value:1},
      {createdAt: new Date(),updatedAt: new Date(),key:"ogTitle",value:""},
      {createdAt: new Date(),updatedAt: new Date(),key:"statisticalCode",value:"https://hm.baidu.com/hm.js?83ae79944c5758d0fae8564927fc3fa0"},
      {createdAt: new Date(),updatedAt: new Date(),key:"siteLogo",value:"/static/themes/dorawhite/images/logo.png"},
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Web_Configs', null, {});
  }
};
