
const { Sequelize, DataTypes, Model, Op } = require('sequelize');
const shortid = require('shortid');
const _ = require('lodash');


class GeneralModel extends Model {
}

/**
 * 通用列表
 * @method list
 * @param  {[type]} req     [description]
 * @param  {[type]} res     [description]
 * @param  {[type]} this [description]
 * @param  {[type]} sort    排序
 * @return {[type]}         [description]
 */
GeneralModel._list = async function({
    fields = null,  // 字段
    where = {},     // 条件
    include = [],  // 外连接
    page = {},    // 页面信息
    search = {},  // 搜索相关信息
    orderMod = null, // 排序模式，值域可以为 'id','create','update'

    offset = null,  // offset ，如果指定，覆盖上面默认值
    limit = null,   // 限制个数，如果指定覆盖上面默认值
    order = null,    // 排序,如果特别指定，覆盖上面默认值
    attributes = null, // 同fields，如果同时不为null,报错返回
}) {

    let {
        current,
        pageSize,
    } = page;

    if (!_.isEmpty(page)){
        current = current || 1;
        pageSize = Number(pageSize) || 10;
        limit = limit || pageSize;
        offset = offset || (((Number(current)) - 1) * Number(pageSize));
    }

    if (order == null && orderMod != null){
        if (orderMod == 'id'){
            order = ['id']
        }
        if (orderMod == 'create'){
            order = ['createAt']
        }
        if (orderMod == 'update'){
            order = ['updateAt', 'DESC']
        }
        if (orderMod == 'sort'){
            order = ['sort']
        }
    }

    if (fields != null && attributes != null){
        throw new Error({ translate : true, message : 'validate_error_params'});
    }
    attributes = attributes || fields;

    
    let {
        searchkey,
        searchKeysInTable
    } = search;
    
    if (!_.isEmpty(search)){

        if (searchkey) {
            searchKeysInTable = searchKeysInTable || this.searchKeysInTable;
            if (searchKeysInTable) {
                if (_.isArray(searchKeysInTable) && searchKeysInTable.length > 0) {
                    let searchStr = [];
                    for (let i = 0; i < searchKeysInTable.length; i++) {
                        const keyItem = searchKeysInTable[i];
                        searchStr.push({
                            [keyItem]: {
                                [Op.regexp]: searchkey
                            }
                        })
                    }
                    where[Op.or] = searchStr;
                } else {
                    where[searchKeysInTable] = {
                        [Op.regexp]: searchkey
                    }
                }
            }
        }
    }
    
     
    console.log('--where--', where);
    
    let { count, rows } = await this.findAndCountAll({where,attributes,offset,limit,order,include});

    if (!_.isEmpty(page)) {

        let pageInfoParams = {
            totalItems: count,
            pageSize: pageSize,
            current: current,
            totalPage: Math.ceil(count / pageSize),
            searchkey: searchkey,
        };
        for (const querykey in where) {
            if (where.hasOwnProperty(querykey)) {
                const queryValue = where[querykey];
                if (typeof queryValue != 'object') {
                    _.assign(pageInfoParams, {
                        [querykey]: queryValue || ''
                    });
                }
            }
        }
        return {
            docs: rows,
            pageInfo: pageInfoParams
        }
    } else {
        return rows;
    }

}

/**
 * 通用删除
 * @method deletes
 * @param  {[type]}   this [description]
 * @param  {[type]}   ids [description]
 */

GeneralModel._removes = async function( ids, key)  {

    if (!checkCurrentId(ids)) {
        throw new Error({ translate : true, message : 'validate_error_params'});
    } else {
        ids = ids.split(',');
    }

    // todo
    //ctx.logger.warn(_addActionUserInfo(ctx, {
    //    ids,
    //    key
    //}));
//

    return this.destroy({
        where: {
          [key]: ids
        }
      });
}

/**
 * 通用删除
 * @method deletes
 * @param  {[type]}   this [description]
 */

GeneralModel._removeAll = async function()  {

    return await this.destroy({
        truncate: true
      })
}


/**
 * 通用编辑
 * @method update
 * @param  {[type]} this [description]
 * @param  {[type]} _id     [description]
 * @param  {[type]} data    [description]
 */

GeneralModel._update = async function (id, data, query = {})  {

    if (id) {
        query = _.assign({}, query, {
            id: id
        });
    } else {
        if (_.isEmpty(query)) {
            throw new Error({ translate : true, message : 'validate_error_params'});
        }
    }

    const user = await this.findOne({
        where: query
    })

    if (_.isEmpty(user)) {
        throw new Error({ translate : true, message : 'validate_error_params'});
    }

    return await this.findOneAndUpdate({
        data
    },{
        where : query
    });

}


/**
 * 通用编辑
 * @method update
 * @param  {[type]} this [description]
 * @param  {[type]} ids     [description]
 * @param  {[type]} data    [description]
 */

GeneralModel._updateMany = async function(ids = '', data, query = {})  {

    if (_.isEmpty(ids) && _.isEmpty(query)) {
        throw new Error({ translate : true, message : 'validate_error_params'});
    }

    if (!_.isEmpty(ids)) {
        if (!checkCurrentId(ids)) {
            throw new Error({ translate : true, message : 'validate_error_params'});
        } else {
            ids = ids.split(',');
        }
        query = _.assign({}, query, {
            _id: {
                $in: ids
            }
        });
    }

    return await this.updateMany(data, {
        where : query
    });

}


/**
 * 通用属性加值
 * @method update
 * @param  {[type]} this [description]
 * @param  {[type]} id     [description]
 * @param  {[type]} data    [description]
 */

GeneralModel._inc = async function (id, data, {
    query = {}
} = {})  {

    if (_.isEmpty(id) && _.isEmpty(query)) {
        throw new Error({ translate : true, message : 'validate_error_params'});
    }

    if (!_.isEmpty(id)) {
        query = _.assign({}, query);
    }
    
    return await this.increment( data, {
        where: query
    });

}


module.exports =  GeneralModel;



// 关键操作记录日志
function _addActionUserInfo (ctx, params = {})  {

    let infoStr = '';

    if (!_.isEmpty(ctx.session.UserInfo)) {
        infoStr += 'actionUser: ' + JSON.stringify(ctx.session.UserInfo) + ',';
    }

    if (!_.isEmpty(params)) {
        infoStr += 'actionParams: ' + JSON.stringify(params) + ',';
    }

    return infoStr;
}
