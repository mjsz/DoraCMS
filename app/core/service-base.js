/*
 * @Author: doramart 
 * @Date: 2019-06-24 13:20:49 
 * @Last Modified by: doramart
 * @Last Modified time: 2019-08-14 16:52:04
 */

'use strict';
const Service = require('egg').Service;


const _ = require('lodash');

class baseService extends Service {
    constructor(...args){
        super(...args);
        this.Model = null;
    }
    async __create(createObj) {
      // 兼容 _id 和 id
      this.compatibleMyMongo4Where(createObj);
      return await this.Model.create(createObj);
    }

    async __item(where = {}, options = {}) {
      
      // 兼容 _id 和 id
      this.compatibleMyMongo4Where(where);
      this.compatibleMyMongo4Option(options);

      options.where = where;
      return this.Model.findOne(options);
    }
    
    async __itemById(id, options = {}) {
      return this.__item({id} ,options);
    }

    async __list(where, options = {}) {
      // 兼容 _id 和 id
      this.compatibleMyMongo4Where(where);
      this.compatibleMyMongo4Option(options);

      options.where = where;
      return this.Model._list(options);
    }

    async __listOfPage(page, where, options = {}) {
      options.page = page;
      return this.__list(where, options);
    }

    async __listBySearch(search, page, where, options = {}) {
      options.search = search;
      options.page = page;
      return this.__list(where, options);
    }

    async __count(where = {}, options = {}) {
      // 兼容 _id 和 id
      this.compatibleMyMongo4Where(where);

      options.where = where;
      return await this.Model.count(options);
    }

    async __update(where, values) {
      // 兼容 _id 和 id
      this.compatibleMyMongo4Where(where);

      return this.Model.update(values, {where});
    }

    async __updateById(id, values) {
      return this.__update({id}, values);
    }
    
    async __updateByIds(ids, values) {
      return this.__update({id:ids}, values);
    }

    async __remove(where) {
      // 兼容 _id 和 id
      this.compatibleMyMongo4Where(where);

      return this.Model.destroy({where});
    }    
    
    async __removeById(id) {
      return this.__remove({id});
    }

    async __removeByIds(ids) {
      return this.__remove({id:ids});
    }

    async __disable(where, values = {state: '0'}) {
      // 兼容 _id 和 id
      this.compatibleMyMongo4Where(where);
      
      return this.Model.update(values, {where});
    }

    async __disableById(id, values = {state: '0'}) {
      return this.__disable({id}, values);
    }

    async __disableByIds(ids, values = {state: '0'}) {
      return this.__disable({id:ids}, values);
    }



    // 兼容性
    
    compatibleMyMongo4List(payload, options){
      // 兼容 _id 和 id
      let rOption = {
        fields : null,  // 字段
        where : {},     // 条件
        include : [],  // 外连接
        page : {},    // 页面信息
        search : {},  // 搜索相关信息
        orderMod : null, // 排序模式，值域可以为 'id','create','update'
    
        offset : null,  // offset ，如果指定，覆盖上面默认值
        limit : null,   // 限制个数，如果指定覆盖上面默认值
        order : null,    // 排序,如果特别指定，覆盖上面默认值
        attributes : null, // 同fields，如果同时不为null,报错返回
      };

      let {
        current,
        pageSize,
        searchkey,
        isPaging,
        skip
      } = payload;
      
      isPaging = isPaging == '0' ? false : true;
      if (isPaging){
        rOption.page.current = current;
        rOption.page.pageSize = pageSize;
      }
      if (searchkey){  
        rOption.search.searchkey = searchkey;
        searchKeysInTable = options.searchKeys;
      }

      if (!isPaging && pageSize){
        rOption.limit = pageSize;
      }
      if (!isPaging && skip){
        rOption.offset = skip;
      }

      rOption.where = options.where||options.query;
      rOption.order = options.order||options.sort;
      rOption.include = options.include||options.populate;

      return [rOption.where, rOption];
    }

    compatibleMyMongo4Where(where){
      // 兼容 _id 和 id
      if (where._id && !where.id){
        where.id = where._id;
      }
    }
    
    compatibleMyMongo4Option(options){
      if (options.files && !options.attributes){
        if (_.isString(options.files)){
          if (options.files[0] == '-'){
            options.attributes = {
              exclude: options.files.substr(1).split(' ')
            }
          }else{
            options.attributes = options.files.split(' ');
          }
        }else{
          options.attributes = options.files;
        }
      }

      // 兼容 _id 和 id
      if (_.isArray(options.attributes)){
        options.attributes.push(['id' ,'_id']);
      }else if(_.isObject(options.attributes)){
        if (options.attributes.include){
          options.attributes.include.push(['id' ,'_id']);
        }else{
          options.attributes = { include: [
            ['id' ,'_id']
          ]}
        }
      }else {
        options.attributes = { include: [
          ['id' ,'_id']
        ]}
      }
    }
}

module.exports = baseService;