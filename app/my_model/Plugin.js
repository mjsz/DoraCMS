
'use strict';

const GeneralModel = require("../core/general_model_my");
var shortid = require('shortid');




class Plugin extends GeneralModel {}


module.exports =  async (app) => {

  const { STRING, INTEGER, DATE, BOOLEAN } = app.Sequelize;
  
  // 定义模型
  let PluginSchema = {
    _id: {
        type: STRING,
        'default': shortid.generate
    },
    pluginId: STRING, // 来自插件源id
    alias: STRING, // 插件别名
    pkgName: STRING, // 包名 
    name: STRING, // 插件名称
    enName: STRING, // 插件英文名
    description: STRING, // 插件描述
    amount: {
        type: INTEGER,
        default: 0
    }, // 价格
    isadm: {
        type: STRING,
        default: '1'
    }, // 有后管 
    isindex: {
        type: STRING,
        default: '0'
    }, // 有前台 
    version: STRING, // 版本号 
    operationInstructions: STRING, // 操作说明
    author: STRING, // 作者
    adminUrl: {
        type: STRING
    }, // 后台插件地址
    iconName: STRING, // 主菜单图标名称
    adminApi: {
        type: STRING
    }, // 后台插件地址
    fontApi: {
        type: STRING
    }, // 后台插件地址
    authUser: {
        type: BOOLEAN, // 是否鉴权用户
        default: false
    },
    initDataPath: STRING, //初始化表路径
    defaultConfig: STRING, //插入到 config.default.js 中的配置
    pluginsConfig: STRING, //插入到 plugins.js 中的配置
    type: {
        type: STRING, // 插件类型
        default: "1"
    },
    installor: {
        type: STRING,
    },
};

  Plugin.init(PluginSchema, {
    // 这是其他模型参数
    sequelize : app.myModel, // 我们需要传递连接实例
    modelName: 'Plugin' // 我们需要选择模型名称
  });

  // 定义关联关系
  if (app.startevent){
    app.startevent.create(async function(){
      let User = app.myModel.models.User;
      Plugin.belongsTo(User, { foreignKey: 'installor', through: 'Link_User_Plugin' }); //插件属于某个用户创建
    });
  }


  
  if (process.env.NODE_ENV == 'development'){
    console.log("Plugin.init");

    if (app.config.isInitMyDb){
      await Plugin.sync({ alter: true });
    }
    if (seedDatas && seedDatas.length > 0){
      const amount = await Plugin.count();
      if (amount == 0){
        await Plugin.bulkCreate(seedDatas);
      }
    }
  }

  return Plugin;
};



let seedDatas = null;
