

'use strict';

const GeneralModel = require("../core/general_model_my");


class WebConfig extends GeneralModel {}


module.exports =  async (app) => {

  const { STRING, INTEGER, DATE } = app.Sequelize;
  
  WebConfig.init({
    key: { type: STRING, unique: 'key' },
    value: STRING
  }, {
    // 这是其他模型参数
    sequelize : app.myModel, // 我们需要传递连接实例
    modelName: 'WebConfig' // 我们需要选择模型名称
  });

  if (process.env.NODE_ENV == 'development'){
    console.log("WebConfig.init");

    if (app.config.isInitMyDb){
      await WebConfig.sync({ alter: true });
    }
    if (seedDatas && seedDatas.length > 0){
      const amount = await WebConfig.count();
      if (amount == 0){
        await WebConfig.bulkCreate(seedDatas);
      }
    }
  }

  return WebConfig;
};


var seedDatas =  [
  {key:"_id",value:"HyH1BsG7W"},
  {key:"siteKeywords",value:"前端开发俱乐部,前端俱乐部,DoraCMS,Nodejs内容管理系统, 前端开发, web前端, 前端开发工程师,前端资源, angularjs, 前端开发工具, nodejs ,boostrap"},
  {key:"siteEmail",value:"522240095@qq.com"},
  {key:"databackForderPath",value:"/home/database/doracms/"},
  {key:"mongodbInstallPath",value:"/usr/local/mongodb/mongodb-linux-x86_64-ubuntu1604-4.0.0/bin/"},
  {key:"registrationNo",value:"粤ICP备15038960号-2"},
  {key:"siteDiscription",value:"前端开发俱乐部,分享前端知识,丰富前端技能。汇集国内专业的前端开发文档,为推动业内前端开发水平共同奋斗。DoraCMS,html,js,css,nodejs,前端开发,jquery,web前端, web前端开发, 前端开发工程师"},
  {key:"siteDomain",value:"https://www.html-js.cn"},
  {key:"siteName",value:"前端开发俱乐部"},
  {key:"__v",value:0},
  {key:"siteEmailPwd",value:"U2FsdGVkX19ipgNmDJ2FjX3r3KsGdutctMSnUxJ88UQCfNehBkGBiSKYVt8KogdV"},
  {key:"siteEmailServer",value:"QQ"},
  {key:"showImgCode",value:1},
  {key:"shareArticlScore",value:1},
  {key:"postMessageScore",value:1},
  {key:"poseArticlScore",value:1},
  {key:"siteAltKeywords",value:"前端开发俱乐部,前端俱乐部,DoraCMS,Nodejs内容管理系统, 前端开发, web前端, 前端开发工程师,前端资源, angularjs, 前端开发工具, nodejs ,boostrap"},
  {key:"bakDataRate",value:"3"},
  {key:"bakDatabyTime",value:1},
  {key:"ogTitle",value:""},
  {key:"statisticalCode",value:"https://hm.baidu.com/hm.js?83ae79944c5758d0fae8564927fc3fa0"},
  {key:"siteLogo",value:"/static/themes/dorawhite/images/logo.png"},
]