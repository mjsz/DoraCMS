
'use strict';

const GeneralModel = require("../core/general_model_my");
var shortid = require('shortid');




class Group extends GeneralModel {}


module.exports =  async (app) => {

  const { STRING, INTEGER, DATE, BOOLEAN } = app.Sequelize;
  
  // 定义模型
  let GroupSchema = {
      id: {
          type: STRING, primaryKey: true, 
          set(value) {
            if (!value){
                this.setDataValue('id', shortid.generate());
            }else{
                this.setDataValue('id', value); 
            }
          }
      },
      name: STRING,
      code :{
          type: STRING,
      },
      comments: STRING
  };

  Group.init(GroupSchema, {
    // 这是其他模型参数
    sequelize : app.myModel, // 我们需要传递连接实例
    modelName: 'Group' // 我们需要选择模型名称
  });

  // 定义关联关系
  if (app.startevent){
    app.startevent.create(async function(){
      let User = app.myModel.models.User;
      User.belongsToMany(Group, { through: 'Link_User_Group' }); //一个用户可以有多个角色
      Group.belongsToMany(User, { through: 'Link_User_Group' }); //多个用户可以是同一个角色

      let AdminResource = app.myModel.models.AdminResource;
      AdminResource.belongsToMany(Group, { through: 'Link_AdminResource_Group' });// 一个角色可以有多个资源权限
      Group.belongsToMany(AdminResource, { through: 'Link_AdminResource_Group' });// 多个角色可以共享同一个资源

          
      if (process.env.NODE_ENV == 'development'){
        console.log("Group.link.init");

        if (app.config.isInitMyDb){
          await app.myModel.models.Link_AdminResource_Group.sync({ alter: true });
          await app.myModel.models.Link_User_Group.sync({ alter: true });
        }

        if (SeedDataU_G && SeedDataU_G.length > 0){
          const amount = await app.myModel.models.Link_User_Group.count();
          if (amount == 0){
            await app.myModel.models.Link_User_Group.bulkCreate(SeedDataU_G);
          }
        }

				SeedDataR_G = [];
				
				for (const key in seedDatas) {
					if (seedDatas.hasOwnProperty(key)) {
						const it = seedDatas[key];
						const GroupId = it.id;
						for (const i in it.power) {
							if (it.power.hasOwnProperty(i)) {
								const AdminResourceId = it.power[i];
								SeedDataR_G.push({GroupId, AdminResourceId});
							}
						}
					}
				}
        
        if (SeedDataR_G && SeedDataR_G.length > 0){
          const amount = await app.myModel.models.Link_AdminResource_Group.count();
          if (amount == 0){
			await app.myModel.models.Link_AdminResource_Group.bulkCreate(SeedDataR_G);
			
			let sSql = `
			insert IGNORE INTO link_adminresource_group (admin_resource_id,group_id)
				select r.parent_id , t.group_id from link_adminresource_group t, admin_resources r 
				where t.admin_resource_id = r.id
				and r.parent_id is not null 
			`;
			await app.myModel.query(sSql);
			await app.myModel.query(sSql);
			await app.myModel.query(sSql);
          }
        }

      }

      
      

    });
  }


  
  if (process.env.NODE_ENV == 'development'){
    console.log("Group.init");

    if (app.config.isInitMyDb){
      await Group.sync({ alter: true });
    }
    if (seedDatas && seedDatas.length > 0){
      const amount = await Group.count();
      if (amount == 0){
        await Group.bulkCreate(seedDatas);
      }
    }
  }

  return Group;
};



let seedDatas = [
{
	"id" : "yIGXaqSwo",
	"power" : [
		"-wxeb8kgHpn",
		"RWAHqmIeU",
		"J9lxuRVWE",
		"Vyg7WKQbA",
		"FD2f2xHOL",
		"qwXeDBVlT",
		"fBQSSebyn",
		"Hjwd5sYIU",
		"6MAc8E1_V",
		"DCKT6YRg",
		"ae-t0wkr",
		"YagSpwXJ",
		"QBhVlWPZ",
		"z5oqnDUB",
		"2fUZgE06",
		"knn9IDU_",
		"wAivhqEVV",
		"2_1cSL_Z_5S",
		"ugMjuQZkk2Q",
		"Dz763h2ox69",
		"ktouRdX_XEI",
		"IJIzUZpoCC-",
		"BQ_sVHYI",
		"IEzE2AaD",
		"_V3xx39y",
		"w_hfCU9z"
	],
	"name" : "普通会员用户",
	"comments" : "就是很普通",
	"open2normaluser" : false,
	"code" : "member"
},
{
	"id" : "ncxYj_bjz",
	"power" : [
		"RWAHqmIeU",
		"J9lxuRVWE",
		"Vyg7WKQbA",
		"FD2f2xHOL",
		"qwXeDBVlT",
		"fBQSSebyn",
		"Hjwd5sYIU",
		"6MAc8E1_V",
		"DCKT6YRg",
		"ae-t0wkr",
		"YagSpwXJ",
		"QBhVlWPZ",
		"z5oqnDUB",
		"2fUZgE06",
		"knn9IDU_",
		"2_1cSL_Z_5S",
		"ugMjuQZkk2Q",
		"Dz763h2ox69",
		"ktouRdX_XEI",
		"IJIzUZpoCC-",
		"BQ_sVHYI",
		"IEzE2AaD",
		"_V3xx39y",
		"w_hfCU9z"
	],
	"open2normaluser" : true,
	"name" : "创世神",
	"comments" : "就是创世神",
},
{
	"id" : "EydZWWiR",
	"name" : "测试用户组",
	"power" : [
		"l9E4g18ep",
		"BJ-cqKEe-",
		"NG4NcVL-B",
		"BkMrhYEe-",
		"2IJDj3eow",
		"Sylk6YVgb",
		"1EuBA3Lx4",
		"5llL0Y8W1",
		"qh5uY72DA",
		"BQfUHxF5byR",
		"pQKBS8AF3og",
		"-wxeb8kgHpn",
		"xzzdMYEUAyG",
		"C_yuiyNvLcc",
		"ryQkU16cZ",
		"MLP8QxL6u",
		"-OgUQh7x1",
		"5tXUFzP9",
		"rxvkFDugE",
		"H5JCjbGmV",
		"RWAHqmIeU",
		"J9lxuRVWE",
		"YagSpwXJ",
		"QBhVlWPZ",
		"wAivhqEVV",
		"2_1cSL_Z_5S",
		"ugMjuQZkk2Q",
		"BQ_sVHYI",
		"Jbm8t44z",
		"pEGIVzw9",
		"fzDo1CCw",
		"-J8RLI5G",
		"s8xB9GwL"
	],
	"comments" : "测试用户组"
},
{
	"id" : "E1XjEmqA",
	"name" : "超级管理员",
	"power" : [
		"S3WpShC3s",
		"g9Yr5Zqhv",
		"2-fR-TWSk",
		"l9E4g18ep",
		"Q2QtldRyb",
		"cvJRdK16m",
		"RYzZPmnoP",
		"5aYlHoqqc",
		"10g3jL8lF",
		"BywRcKNxW",
		"rJFGsF4g-",
		"BJ-cqKEe-",
		"BycritEeW",
		"NG4NcVL-B",
		"BkMrhYEe-",
		"BkrPnF4lW",
		"Hyg52FEgZ",
		"Syk2hFVlZ",
		"2IJDj3eow",
		"Sylk6YVgb",
		"Bk3DAtNgW",
		"H10KCFElW",
		"1EuBA3Lx4",
		"HJVnRK4e-",
		"JQ1F8t5me",
		"SJ9MQFqEW",
		"rJKNXFqEb",
		"5llL0Y8W1",
		"qh5uY72DA",
		"XSDO8imWB",
		"KXLs5rzbj",
		"TKJBY1m13",
		"1vNd58tjI",
		"42yBrE5Nc",
		"wEWjpeYHv",
		"paeyOM57w",
		"vLScv8oRj",
		"BQfUHxF5byR",
		"pQKBS8AF3og",
		"0QCOzomfQlT",
		"-wxeb8kgHpn",
		"MwuIagAPIsP",
		"CDyzNO0rdXY",
		"xzzdMYEUAyG",
		"C_yuiyNvLcc",
		"m5eGDR9zCB-",
		"hlUndgufsHn",
		"R3DggpO2MQZ",
		"5hkbZDmYV1h",
		"ryQkU16cZ",
		"MLP8QxL6u",
		"-OgUQh7x1",
		"5tXUFzP9",
		"awLVLy13",
		"P5JEX8J4",
		"O0uM6EPr",
		"AehsXQ3z",
		"rxvkFDugE",
		"QHVTAhIlt",
		"nxvEf6haP",
		"4Lsfjh1o4",
		"H5JCjbGmV",
		"Jp_o65bZo",
		"XypX71QEU",
		"RWAHqmIeU",
		"J9lxuRVWE",
		"Vyg7WKQbA",
		"FD2f2xHOL",
		"qwXeDBVlT",
		"fBQSSebyn",
		"Hjwd5sYIU",
		"6MAc8E1_V",
		"DCKT6YRg",
		"ae-t0wkr",
		"YagSpwXJ",
		"QBhVlWPZ",
		"z5oqnDUB",
		"2fUZgE06",
		"knn9IDU_",
		"wAivhqEVV",
		"2_1cSL_Z_5S",
		"ugMjuQZkk2Q",
		"Dz763h2ox69",
		"ktouRdX_XEI",
		"IJIzUZpoCC-",
		"BQ_sVHYI",
		"IEzE2AaD",
		"_V3xx39y",
		"w_hfCU9z",
		"KJ7OHV7Pf76",
		"5EKmTw0_Fbb",
		"dV6YPgL-n7q",
		"EbENuFg5dsi",
		"Jbm8t44z",
		"pEGIVzw9",
		"Jg6GoxDB",
		"TQpAA5ip",
		"dslb7_bj",
		"fzDo1CCw",
		"-J8RLI5G",
		"s8xB9GwL",
		"Z2bJzBq5",
		"VSXTj1w9",
		"zlHstOLA",
		"_lwy1SDq"
	],
	"enable" : true,
	"comments" : "超级管理员",
	"open2normaluser" : false,
	"code" : "superadmin"
}]
;


let SeedDataU_G = [
{
  "UserId": "ft0bGHVwcA",
  "GroupId": "yIGXaqSwo",
},
{
  "UserId": "T7XcLJaCv",
  "GroupId": "yIGXaqSwo",
},
{
  "UserId": "B1OTQRbOW",
  "GroupId": "EydZWWiR",
},
{
  "UserId": "4JiWCMhzg",
  "GroupId": "E1XjEmqA",
}
]
;

let SeedDataR_G = null;