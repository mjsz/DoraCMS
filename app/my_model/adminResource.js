

'use strict';

const GeneralModel = require("../core/general_model_my");
var shortid = require('shortid');




class AdminResource extends GeneralModel {}


module.exports = async (app) => {

  const { STRING, INTEGER, DATE, BOOLEAN } = app.Sequelize;
  
  AdminResource.init({
    id: { type: STRING, primaryKey: true, set(value) {
            if (!value){
                this.setDataValue('id', shortid.generate());
            }else{
                this.setDataValue('id', value); 
            }
          }
        },
    parentId: STRING,                             // 父节点
    comments: STRING,                             // 说明
    type: STRING,                                 // 0为普通菜单 1为功能
    enable: { type:BOOLEAN,defaultValue: true},   // 是否可配置
    sort: INTEGER,                                // 排序
    apiPath :{type : STRING, allowNull : false},  // 直接就是url啦，下面两个删掉
    routePath: STRING,                            // URL 菜单Menu  ,早晚要去掉，这个不仅仅是路径，还是什么app名称，都是凑插件弄得
    //api: { type: STRING},                         // URL 操作API
    icon: STRING,                                 // 菜单ICON
    label: STRING,                                // 名称，后台管理 app 的名称
    isWriteList : { type:BOOLEAN, defaultValue: false}, // 是否是白名单功能

    isExt: { type:BOOLEAN, defaultValue: false},  // 是否是插件 , 也不用啦，不用关心是否是插件的
    componentPath: STRING,                        // 模板路径   ，好像没啥用
    v:INTEGER
  }, {
    // 这是其他模型参数
    sequelize : app.myModel, // 我们需要传递连接实例
    modelName: 'AdminResource' // 我们需要选择模型名称
  });

  if (process.env.NODE_ENV == 'development'){
    console.log("AdminResource.init");

    if (app.config.isInitMyDb){
      await AdminResource.sync({ alter: true });
    }
    if (seedDatas && seedDatas.length > 0){
      const amount = await AdminResource.count();
      if (amount == 0){
        await AdminResource.bulkCreate(seedDatas);
      }
    }
  }

  return AdminResource;
};


var seedDatas = [
{
  "id": "zlHstOLA",
  "isExt": false,
  "enable": true,
  "sort": 5,
  "label": "",
  "type": "1",
  "apiPath": "/manage/mailDelivery/delete",
  "parentId": "G5V9Tgv8",
  "icon": "",
  "comments": "删除发送任务",
  "date": "2020-02-13T12:35:32.681Z",
  "v": 0
},
{
  "id": "z5oqnDUB",
  "isExt": false,
  "enable": true,
  "sort": 3,
  "label": "",
  "type": "1",
  "apiPath": "/manage/contentCategory/addOne",
  "parentId": "R9gC7qTn",
  
  "icon": "",
  
  "comments": "添加单个类别",
  "date": "2020-02-21T07:38:33.219Z",
  "v": 0
},
{
  "id": "yYPI2NKGT",
  "isExt": true,
  "enable": true,
  "sort": 1,
  "label": "contentMessageList",
  "type": "1",
  "apiPath": "/manage/contentMessage/getList",
  "parentId": "MKqeoofX",
  
  "icon": "",
  
  "comments": "获取留言列表",
  "date": "2020-03-09T10:55:39.515Z",
  "v": 0
},
{
  "id": "xzzdMYEUAyG",
  "isExt": true,
  "enable": true,
  "sort": 4,
  "label": "templateConfigGetTempItemForderList",
  "type": "1",
  "apiPath": "/manage/template/getTemplateItemlist",
  "parentId": "jok29F8KMye",
  
  "icon": "",
  
  "comments": "获取默认模板的模板单元列表",
  "date": "2019-09-29T07:04:57.659Z",
  "v": 0
},
{
  "id": "xKoo9klP",
  "isExt": true,
  "enable": true,
  "sort": 21,
  "label": "versionManageManage",
  "type": "0",
  
  "parentId": "SkFHdYElb",
  "apiPath": "/admin/versionManage",
  "routePath": "versionManage",

  "icon": "icon_app",
  "componentPath": "version/admin/index",
  "comments": "app版本",
  "date": "2019-12-10T11:00:41.306Z",
  "v": 0
},
{
  "id": "wnIQRCGp",
  "isExt": true,
  "enable": true,
  "sort": 22,
  "label": "announceManage",
  "type": "0",
  
  "parentId": "SkFHdYElb",
  "apiPath": "/admin/announce",
  "routePath": "announce",

  "icon": "icon_horn",
  "componentPath": "announce/index",
  "comments": "系统公告",
  "date": "2020-01-10T02:27:17.230Z",
  "v": 0
},
{
  "id": "w_hfCU9z",
  "isExt": false,
  "enable": true,
  "sort": 4,
  "label": "",
  "type": "1",
  "apiPath": "/manage/contentMessage/deleteMessage",
  "parentId": "1Nt0WrJ1",
  
  "icon": "",
  
  "comments": "删除留言",
  "date": "2020-03-04T07:55:18.050Z",
  "v": 0
},
{
  "id": "wKU6UC0Bp",
  "isExt": false,
  "enable": true,
  "sort": 3,
  "label": "",
  "type": "0",
  
  "parentId": "0",
  "apiPath": "/admin/userManage",
  "routePath": "userManage",

  "icon": "icon_people_fill",
  
  "comments": "会员管理",
  "date": "2019-09-29T07:07:43.104Z",
  "v": 0
},
{
  "id": "wEWjpeYHv",
  "isExt": true,
  "enable": true,
  "sort": 3,
  "label": "helpCenterCreate",
  "type": "1",
  "apiPath": "/manage/helpCenter/addOne",
  "parentId": "-O2jAnfo",
  
  "icon": "",
  
  "comments": "添加单个帮助",
  "date": "2020-02-19T04:30:49.972Z",
  "v": 0
},
{
  "id": "wAivhqEVV",
  "isExt": false,
  "enable": true,
  "sort": 6,
  "label": "",
  "type": "1",
  "apiPath": "/manage/contentCategory/starCategory",
  "parentId": "R9gC7qTn",
  
  "icon": "",
  
  "comments": "特别标记类别",
  "date": "2020-05-25T05:58:44.474Z",
  "v": 0
},
{
  "id": "vbqzEsl_",
  "isExt": true,
  "enable": true,
  "sort": 10,
  "label": "adsManage",
  "type": "0",
  
  "parentId": "SkFHdYElb",
  "apiPath": "/admin/ads",
  "routePath": "ads",

  "icon": "icon_shakehands_fill",
  "componentPath": "ads/index",
  "comments": "广告管理",
  "date": "2020-02-19T03:45:34.823Z",
  "v": 0
},
{
  "id": "vLScv8oRj",
  "isExt": true,
  "enable": true,
  "sort": 5,
  "label": "helpCenterRemoves",
  "type": "1",
  "apiPath": "/manage/helpCenter/delete",
  "parentId": "-O2jAnfo",
  
  "icon": "",
  
  "comments": "删除帮助",
  "date": "2020-02-19T04:30:49.980Z",
  "v": 0
},
{
  "id": "ugMjuQZkk2Q",
  "isExt": true,
  "enable": true,
  "sort": 2,
  "label": "contentTagsGetOne",
  "type": "1",
  "apiPath": "/manage/contentTag/getOne",
  "parentId": "9hvFygZHo_F",
  
  "icon": "",
  
  "comments": "获取单条标签信息",
  "date": "2019-09-29T07:04:57.530Z",
  "v": 0
},
{
  "id": "s8xB9GwL",
  "isExt": false,
  "enable": true,
  "sort": 2,
  "label": "",
  "type": "1",
  "apiPath": "/manage/mailDelivery/getOne",
  "parentId": "G5V9Tgv8",
  
  "icon": "",
  
  "comments": "获取单个发送信息",
  "date": "2020-02-13T12:34:36.460Z",
  "v": 0
},
{
  "id": "ryQkU16cZ",
  "label": "getSysTermBaseInfo",
  "type": "1",
  "apiPath": "/manage/getSitBasicInfo",
  "parentId": "r1fTSk65-",
  
  "icon": "",
  
  "comments": "获取站点基础信息",
  "date": "2017-09-18T06:21:14.788Z",
  "sort": 1,
  "enable": true,
  "v": 0
},
{
  "id": "rxvkFDugE",
  "isExt": true,
  "enable": true,
  "sort": 1,
  "label": "versionManageList",
  "type": "1",
  "apiPath": "/manage/version/admin/getList",
  "parentId": "xKoo9klP",
  
  "icon": "",
  
  "comments": "获取版本配置",
  "date": "2019-12-10T11:00:41.312Z",
  "v": 0
},
{
  "id": "rkI6YtEgW",
  "v": 0,
  "comments": "资源管理",
  "date": "2017-05-13T13:27:25.717Z",
  "label": "resourceManage",
  "parentId": "SkFHdYElb",
  "sort": 3,
  "type": "0",
  "apiPath": "/admin/adminResource",
  "routePath": "adminResource",
  "componentPath": "adminResource/index",
  "enable": true,
  "icon": "icon_power"
},
{
  "id": "rJKNXFqEb",
  "label": "updateSystemConfig",
  "type": "1",
  "apiPath": "/manage/systemConfig/updateConfig",
  "parentId": "HkHTzKcN-",
  
  
  "comments": "更新系统信息",
  "date": "2017-07-05T15:18:08.600Z",
  "sort": 2,
  "enable": true,
  "v": 0,
  "icon": ""
},
{
  "id": "rJFGsF4g-",
  "v": 0,
  "apiPath": "/manage/adminUser/updateOne",
  "comments": "用户更新操作",
  "date": "2017-05-13T13:33:04.816Z",
  "label": "updateAdminUser",
  "parentId": "H1kjttNg-",
  "sort": 1,
  "type": "1",
  "enable": true,
  
  "icon": "",
  "apiPath": "/admin/"
},
{
  "id": "r1fTSk65-",
  "label": "sysTemMain",
  "type": "0",
  
  "parentId": "SkFHdYElb",
  "apiPath": "/admin/main",
  "routePath": "main",

  "icon": "",
  "componentPath": "main/index",
  "comments": "系统主页",
  "date": "2017-09-18T06:20:42.270Z",
  "sort": 14,
  "enable": false,
  "v": 0
},
{
  "id": "qwXeDBVlT",
  "isExt": true,
  "enable": true,
  "sort": 5,
  "label": "contentRemoves",
  "type": "1",
  "apiPath": "/manage/content/deleteContent",
  "parentId": "_KLwaD8s",
  
  "icon": "",
  
  "comments": "删除文档",
  "date": "2019-12-18T08:13:24.520Z",
  "v": 0
},
{
  "id": "qh5uY72DA",
  "isExt": true,
  "enable": true,
  "sort": 2,
  "label": "adsGetOne",
  "type": "1",
  "apiPath": "/manage/ads/getOne",
  "parentId": "vbqzEsl_",
  
  "icon": "",
  
  "comments": "获取单条广告信息",
  "date": "2020-02-19T03:45:34.832Z",
  "v": 0
},
{
  "id": "paeyOM57w",
  "isExt": true,
  "enable": true,
  "sort": 4,
  "label": "helpCenterUpdate",
  "type": "1",
  "apiPath": "/manage/helpCenter/updateOne",
  "parentId": "-O2jAnfo",
  
  "icon": "",
  
  "comments": "更新帮助信息",
  "date": "2020-02-19T04:30:49.975Z",
  "v": 0
},
{
  "id": "pQKBS8AF3og",
  "isExt": true,
  "enable": true,
  "sort": 2,
  "label": "contentTempGetFileInfo",
  "type": "1",
  "apiPath": "/manage/template/getTemplateFileText",
  "parentId": "ar1tFGyQeTY",
  
  "icon": "",
  
  "comments": "读取文件内容",
  "date": "2019-09-29T07:04:57.549Z",
  "v": 0
},
{
  "id": "pEGIVzw9",
  "isExt": false,
  "enable": true,
  "sort": 2,
  "label": "",
  "type": "1",
  "apiPath": "/manage/mailTemplate/getOne",
  "parentId": "fD--BHfJ",
  
  "icon": "",
  
  "comments": "获取单个邮件模板",
  "date": "2020-02-13T12:32:23.196Z",
  "v": 0
},
{
  "id": "nxvEf6haP",
  "isExt": true,
  "enable": true,
  "sort": 1,
  "label": "siteMessageList",
  "type": "1",
  "apiPath": "/manage/siteMessage/getList",
  "parentId": "LlI_t5Pa",
  
  "icon": "",
  
  "comments": "获取站点消息列表",
  "date": "2020-01-05T04:28:05.211Z",
  "v": 0
},
{
  "id": "m97HZylz",
  "isExt": true,
  "enable": true,
  "sort": 0,
  "label": "backUpDataManage",
  "type": "0",
  
  "parentId": "SkFHdYElb",
  "apiPath": "/admin/backUpData",
  "routePath": "backUpData",

  "icon": "icon_cspace",
  "componentPath": "backUpData/index",
  "comments": "数据管理",
  "date": "2020-03-11T09:41:31.338Z",
  "v": 0
},
{
  "id": "m5eGDR9zCB-",
  "isExt": true,
  "enable": true,
  "sort": 6,
  "label": "templateConfigInstallTemp",
  "type": "1",
  "apiPath": "/manage/template/installTemp",
  "parentId": "jok29F8KMye",
  
  "icon": "",
  
  "comments": "安装模板",
  "date": "2019-09-29T07:04:57.664Z",
  "v": 0
},
{
  "id": "l9E4g18ep",
  "isExt": true,
  "enable": true,
  "sort": 1,
  "label": "systemOptionLogList",
  "type": "1",
  "apiPath": "/manage/systemOptionLog/getList",
  "parentId": "YrBlpVav",
  
  "icon": "",
  
  "comments": "获取日志列表",
  "date": "2020-02-26T11:16:48.240Z",
  "v": 0
},
{
  "id": "ktouRdX_XEI",
  "isExt": true,
  "enable": true,
  "sort": 4,
  "label": "contentTagsUpdate",
  "type": "1",
  "apiPath": "/manage/contentTag/updateOne",
  "parentId": "9hvFygZHo_F",
  
  "icon": "",
  
  "comments": "更新标签信息",
  "date": "2019-09-29T07:04:57.534Z",
  "v": 0
},
{
  "id": "knn9IDU_",
  "isExt": false,
  "enable": true,
  "sort": 5,
  "label": "",
  "type": "1",
  "apiPath": "/manage/contentCategory/deleteCategory",
  "parentId": "R9gC7qTn",
  
  "icon": "",
  
  "comments": "删除类别",
  "date": "2020-02-21T07:39:11.029Z",
  "v": 0
},
{
  "id": "jok29F8KMye",
  "isExt": true,
  "enable": true,
  "sort": 13,
  "label": "templateConfigManage",
  "type": "0",
  
  "parentId": "SkFHdYElb",
  "apiPath": "/admin/templateConfig",
  "routePath": "templateConfig",

  "icon": "icon_skin",
  "componentPath": "templateConfig/index",
  "comments": "模板配置",
  "date": "2019-09-29T07:04:57.651Z",
  "v": 0
},
{
  "id": "hlUndgufsHn",
  "isExt": true,
  "enable": true,
  "sort": 7,
  "label": "templateConfigUploadCMSTemplate",
  "type": "1",
  "apiPath": "/manage/template/uploadCMSTemplate",
  "parentId": "jok29F8KMye",
  
  "icon": "",
  
  "comments": "上传自定义模板",
  "date": "2019-09-29T07:04:57.667Z",
  "v": 0
},
{
  "id": "g9Yr5Zqhv",
  "isExt": true,
  "enable": true,
  "sort": 2,
  "label": "systemNotifySetMessageHasRead",
  "type": "1",
  "apiPath": "/manage/systemNotify/setHasRead",
  "parentId": "W1kIt2Vb",
  
  "icon": "",
  
  "comments": "设为已读消息",
  "date": "2020-02-21T05:45:31.270Z",
  "v": 0
},
{
  "id": "fzDo1CCw",
  "isExt": false,
  "enable": true,
  "sort": 6,
  "label": "",
  "type": "1",
  "apiPath": "/manage/mailTemplate/getTypeList",
  "parentId": "fD--BHfJ",
  
  "icon": "",
  
  "comments": "获取模板类型列表",
  "date": "2020-02-13T12:33:48.756Z",
  "v": 0
},
{
  "id": "fD--BHfJ",
  "isExt": false,
  "enable": true,
  "sort": 1,
  "label": "mailTemplate",
  "type": "0",
  
  "parentId": "BzVRSYXu",
  "apiPath": "/admin/mailTemplate",
  "routePath": "mailTemplate",

  "icon": "icon_dmail",
  
  "comments": "邮件模板",
  "date": "2020-02-13T12:31:07.450Z",
  "v": 0
},
{
  "id": "fBQSSebyn",
  "isExt": true,
  "enable": true,
  "sort": 6,
  "label": "contentUpdateContentToTop",
  "type": "1",
  "apiPath": "/manage/content/topContent",
  "parentId": "_KLwaD8s",
  
  "icon": "",
  
  "comments": "文档推荐",
  "date": "2019-12-18T08:13:24.523Z",
  "v": 0
},
{
  "id": "dslb7_bj",
  "isExt": false,
  "enable": true,
  "sort": 5,
  "label": "",
  "type": "1",
  "apiPath": "/manage/mailTemplate/delete",
  "parentId": "fD--BHfJ",
  
  "icon": "",
  
  "comments": "删除邮件模板",
  "date": "2020-02-13T12:33:23.403Z",
  "v": 0
},
{
  "id": "dV6YPgL-n7q",
  "isExt": true,
  "enable": true,
  "sort": 3,
  "label": "regUserUpdate",
  "type": "1",
  "apiPath": "/manage/regUser/updateOne",
  "parentId": "3BJ23iVRM3r",
  
  "icon": "",
  
  "comments": "更新会员信息",
  "date": "2019-09-29T07:04:57.596Z",
  "v": 0
},
{
  "id": "cvJRdK16m",
  "isExt": true,
  "enable": true,
  "sort": 3,
  "label": "systemOptionLogRemoveAll",
  "type": "1",
  "apiPath": "/manage/systemOptionLog/deleteAllLogItem",
  "parentId": "YrBlpVav",
  
  "icon": "",
  
  "comments": "清空日志",
  "date": "2020-02-26T11:16:48.245Z",
  "v": 0
},
{
  "id": "bL1dr2m2",
  "isExt": false,
  "enable": true,
  "sort": 20,
  "label": "uploadFile",
  "type": "0",
  
  "parentId": "SkFHdYElb",
  "apiPath": "/admin/uploadFile",
  "routePath": "uploadFile",

  "icon": "icon_file_upload",
  
  "comments": "文件上传",
  "date": "2019-11-21T08:42:44.168Z",
  "v": 0
},
{
  "id": "awLVLy13",
  "isExt": false,
  "enable": true,
  "sort": 2,
  "label": "",
  "type": "1",
  "apiPath": "/manage/plugin/installPlugin",
  "parentId": "_FgnLuIS",
  
  "icon": "",
  
  "comments": "安装插件",
  "date": "2019-10-25T17:01:41.416Z",
  "v": 0
},
{
  "id": "ar1tFGyQeTY",
  "isExt": true,
  "enable": true,
  "sort": 12,
  "label": "contentTempManage",
  "type": "0",
  
  "parentId": "SkFHdYElb",
  "apiPath": "/admin/contentTemp",
  "routePath": "contentTemp",

  "icon": "icon_compile",
  "componentPath": "contentTemp/index",
  "comments": "模板编辑",
  "date": "2019-09-29T07:04:57.545Z",
  "v": 0
},
{
  "id": "ae-t0wkr",
  "isExt": false,
  "enable": true,
  "sort": 11,
  "label": "",
  "type": "1",
  "apiPath": "/manage/content/updateContents",
  "parentId": "_KLwaD8s",
  
  "icon": "",
  
  "comments": "文档批量更新(回收站)",
  "date": "2020-03-11T08:15:07.195Z",
  "v": 0
},
{
  "id": "_lwy1SDq",
  "isExt": false,
  "enable": true,
  "sort": 6,
  "label": "",
  "type": "1",
  "apiPath": "/manage/mailDelivery/getSendLogList",
  "parentId": "G5V9Tgv8",
  
  "icon": "",
  
  "comments": "查询发送日志",
  "date": "2020-02-13T12:35:51.972Z",
  "v": 0
},
{
  "id": "_V3xx39y",
  "isExt": false,
  "enable": true,
  "sort": 3,
  "label": "",
  "type": "1",
  "apiPath": "/manage/contentMessage/addOne",
  "parentId": "1Nt0WrJ1",
  
  "icon": "",
  
  "comments": "添加留言",
  "date": "2020-03-04T07:53:38.195Z",
  "v": 0
},
{
  "id": "_KLwaD8s",
  "isExt": true,
  "enable": true,
  "sort": 1,
  "label": "contentManage",
  "type": "0",
  
  "parentId": "Zt6X0SMVF",
  "apiPath": "/admin/content",
  "routePath": "content",

  "icon": "icon_doc",
  "componentPath": "content/index",
  "comments": "文档管理",
  "date": "2019-12-18T08:13:24.499Z",
  "v": 0
},
{
  "id": "_FgnLuIS",
  "isExt": false,
  "enable": true,
  "sort": 15,
  "label": "plugin",
  "type": "0",
  
  "parentId": "SkFHdYElb",
  "apiPath": "/admin/plugin",
  "routePath": "plugin",

  "icon": "icon_render",
  
  "comments": "插件中心",
  "date": "2019-10-25T17:00:52.851Z",
  "v": 0
},
{
  "id": "Zt6X0SMVF",
  "isExt": false,
  "enable": true,
  "sort": 2,
  "label": "",
  "type": "0",
  
  "parentId": "0",
  "apiPath": "/admin/contentManage",
  "routePath": "contentManage",

  "icon": "icon_doc_fill",
  
  "comments": "文档管理",
  "date": "2019-09-29T07:07:06.636Z",
  "v": 0
},
{
  "id": "Z2bJzBq5",
  "isExt": false,
  "enable": true,
  "sort": 3,
  "label": "",
  "type": "1",
  "apiPath": "/manage/mailDelivery/addOne",
  "parentId": "G5V9Tgv8",
  
  "icon": "",
  
  "comments": "新增发送任务",
  "date": "2020-02-13T12:34:54.370Z",
  "v": 0
},
{
  "id": "YrBlpVav",
  "isExt": true,
  "enable": true,
  "sort": 0,
  "label": "systemOptionLogManage",
  "type": "0",
  
  "parentId": "SkFHdYElb",
  "apiPath": "/admin/systemOptionLog",
  "routePath": "systemOptionLog",

  "icon": "icon_logs",
  "componentPath": "systemOptionLog/index",
  "comments": "系统日志",
  "date": "2020-02-26T11:16:48.235Z",
  "v": 0
},
{
  "id": "YagSpwXJ",
  "isExt": false,
  "enable": true,
  "sort": 1,
  "label": "",
  "type": "1",
  "apiPath": "/manage/contentCategory/getList",
  "parentId": "R9gC7qTn",
  
  "icon": "",
  
  "comments": "获取类别列表",
  "date": "2020-02-21T07:37:53.693Z",
  "v": 0
},
{
  "id": "XypX71QEU",
  "isExt": true,
  "enable": true,
  "sort": 3,
  "label": "announceRemoves",
  "type": "1",
  "apiPath": "/manage/systemAnnounce/deleteItem",
  "parentId": "wnIQRCGp",
  
  "icon": "",
  
  "comments": "删除公告",
  "date": "2020-01-10T02:27:17.244Z",
  "v": 0
},
{
  "id": "XSDO8imWB",
  "isExt": true,
  "enable": true,
  "sort": 3,
  "label": "adsCreate",
  "type": "1",
  "apiPath": "/manage/ads/addOne",
  "parentId": "vbqzEsl_",
  
  "icon": "",
  
  "comments": "添加单个广告",
  "date": "2020-02-19T03:45:34.841Z",
  "v": 0
},
{
  "id": "W1kIt2Vb",
  "isExt": true,
  "enable": true,
  "sort": 0,
  "label": "systemNotifyManage",
  "type": "0",
  
  "parentId": "SkFHdYElb",
  "apiPath": "/admin/systemNotify",
  "routePath": "systemNotify",

  "icon": "icon_system_notic",
  "componentPath": "systemNotify/index",
  "comments": "系统消息",
  "date": "2020-02-21T05:45:31.262Z",
  "v": 0
},
{
  "id": "Vyg7WKQbA",
  "isExt": true,
  "enable": true,
  "sort": 3,
  "label": "contentCreate",
  "type": "1",
  "apiPath": "/manage/content/addOne",
  "parentId": "_KLwaD8s",
  
  "icon": "",
  
  "comments": "添加单个文档",
  "date": "2019-12-18T08:13:24.512Z",
  "v": 0
},
{
  "id": "VSXTj1w9",
  "isExt": false,
  "enable": true,
  "sort": 4,
  "label": "",
  "type": "1",
  "apiPath": "/manage/mailDelivery/updateOne",
  "parentId": "G5V9Tgv8",
  
  "icon": "",
  
  "comments": "修改发送任务",
  "date": "2020-02-13T12:35:13.121Z",
  "v": 0
},
{
  "id": "TQpAA5ip",
  "isExt": false,
  "enable": true,
  "sort": 4,
  "label": "",
  "type": "1",
  "apiPath": "/manage/mailTemplate/updateOne",
  "parentId": "fD--BHfJ",
  
  "icon": "",
  
  "comments": "修改邮件模板",
  "date": "2020-02-13T12:33:04.049Z",
  "v": 0
},
{
  "id": "TKJBY1m13",
  "isExt": true,
  "enable": true,
  "sort": 5,
  "label": "adsRemoves",
  "type": "1",
  "apiPath": "/manage/ads/delete",
  "parentId": "vbqzEsl_",
  
  "icon": "",
  
  "comments": "删除广告",
  "date": "2020-02-19T03:45:34.855Z",
  "v": 0
},
{
  "id": "Sylk6YVgb",
  "v": 0,
  "apiPath": "/manage/adminResource/getList",
  "comments": "获取资源列表",
  "date": "2017-05-13T13:40:39.518Z",
  "label": "getResourceList",
  "parentId": "rkI6YtEgW",
  "sort": 1,
  "type": "1",
  "enable": true,
  
  "icon": "",
  "apiPath": "/admin/"
},
{
  "id": "Syk2hFVlZ",
  "v": 0,
  "apiPath": "/manage/adminGroup/deleteGroup",
  "comments": "删除角色功能",
  "date": "2017-05-13T13:39:50.942Z",
  "label": "delRoles",
  "parentId": "BJmnKKVlZ",
  "sort": 4,
  "type": "1",
  "enable": true,
  
  "icon": "",
  "apiPath": "/admin/"
},
{
  "id": "SkFHdYElb",
  "v": 0,
  "comments": "系统管理",
  "date": "2017-05-13T13:21:04.780Z",
  "label": "systemManage",
  "parentId": "0",
  "sort": 1,
  "type": "0",
  "enable": true,
  
  "icon": "icon_work_fill",
  "apiPath": "/admin/systemManage",
  "routePath": "systemManage",

  "apiPath": "/manage/"
},
{
  "id": "SJ9MQFqEW",
  "label": "getSystemConfig",
  "type": "1",
  "apiPath": "/manage/systemConfig/getConfig",
  "parentId": "HkHTzKcN-",
  
  
  "comments": "获取系统信息",
  "date": "2017-07-05T15:17:38.140Z",
  "sort": 1,
  "enable": true,
  "v": 0,
  "icon": ""
},
{
  "id": "S3WpShC3s",
  "isExt": true,
  "enable": true,
  "sort": 1,
  "label": "systemNotifyList",
  "type": "1",
  "apiPath": "/manage/systemNotify/getList",
  "parentId": "W1kIt2Vb",
  
  "icon": "",
  
  "comments": "获取系统消息列表",
  "date": "2020-02-21T05:45:31.266Z",
  "v": 0
},
{
  "id": "RYzZPmnoP",
  "isExt": true,
  "enable": true,
  "sort": 1,
  "label": "backUpDataList",
  "type": "1",
  "apiPath": "/manage/backupData/admin/getBakList",
  "parentId": "m97HZylz",
  
  "icon": "",
  
  "comments": "获取备份数据列表",
  "date": "2020-03-11T09:41:31.343Z",
  "v": 0
},
{
  "id": "RWAHqmIeU",
  "isExt": true,
  "enable": true,
  "sort": 1,
  "label": "contentList",
  "type": "1",
  "apiPath": "/manage/content/getList",
  "parentId": "_KLwaD8s",
  
  "icon": "",
  
  "comments": "获取文档列表",
  "date": "2019-12-18T08:13:24.503Z",
  "v": 0
},
{
  "id": "R9gC7qTn",
  "isExt": false,
  "enable": true,
  "sort": 2,
  "label": "contentCategory",
  "type": "0",
  
  "parentId": "Zt6X0SMVF",
  "apiPath": "/admin/contentCategory",
  "routePath": "contentCategory",

  "icon": "icon_category",
  
  "comments": "文档类别",
  "date": "2020-02-21T07:37:27.117Z",
  "v": 0
},
{
  "id": "R3DggpO2MQZ",
  "isExt": true,
  "enable": true,
  "sort": 8,
  "label": "templateConfigEnableTemp",
  "type": "1",
  "apiPath": "/manage/template/enableTemp",
  "parentId": "jok29F8KMye",
  
  "icon": "",
  
  "comments": "上传自定义模板",
  "date": "2019-09-29T07:04:57.669Z",
  "v": 0
},
{
  "id": "QHVTAhIlt",
  "isExt": true,
  "enable": true,
  "sort": 2,
  "label": "versionManageUpdate",
  "type": "1",
  "apiPath": "/manage/version/admin/updateOne",
  "parentId": "xKoo9klP",
  
  "icon": "",
  
  "comments": "更新版本信息",
  "date": "2019-12-10T11:00:41.318Z",
  "v": 0
},
{
  "id": "QBhVlWPZ",
  "isExt": false,
  "enable": true,
  "sort": 2,
  "label": "",
  "type": "1",
  "apiPath": "/manage/contentCategory/getOne",
  "parentId": "R9gC7qTn",
  
  "icon": "",
  
  "comments": "获取单条类别信息",
  "date": "2020-02-21T07:38:15.897Z",
  "v": 0
},
{
  "id": "Q2QtldRyb",
  "isExt": true,
  "enable": true,
  "sort": 2,
  "label": "systemOptionLogRemoves",
  "type": "1",
  "apiPath": "/manage/systemOptionLog/deleteLogItem",
  "parentId": "YrBlpVav",
  
  "icon": "",
  
  "comments": "删除单条日志",
  "date": "2020-02-26T11:16:48.243Z",
  "v": 0
},
{
  "id": "P5JEX8J4",
  "isExt": false,
  "enable": true,
  "sort": 3,
  "label": "",
  "type": "1",
  "apiPath": "/manage/plugin/unInstallPlugin",
  "parentId": "_FgnLuIS",
  
  "icon": "",
  
  "comments": "卸载插件",
  "date": "2019-10-25T17:02:01.877Z",
  "v": 0
},
{
  "id": "O0uM6EPr",
  "isExt": false,
  "enable": true,
  "sort": 1,
  "label": "",
  "type": "1",
  "apiPath": "/manage/uploadFile/getList",
  "parentId": "bL1dr2m2",
  
  "icon": "",
  
  "comments": "获取上传配置",
  "date": "2019-11-21T08:43:07.427Z",
  "v": 0
},
{
  "id": "NG4NcVL-B",
  "label": "getOneAdminUser",
  "type": "1",
  "apiPath": "/manage/adminUser/getOne",
  "parentId": "H1kjttNg-",
  
  "icon": "",
  
  "comments": "查询单个用户",
  "date": "2019-07-25T05:26:47.004Z",
  "sort": 5,
  "enable": true,
  "v": 0
},
{
  "id": "MwuIagAPIsP",
  "isExt": true,
  "enable": true,
  "sort": 2,
  "label": "templateConfigAddTemplateItem",
  "type": "1",
  "apiPath": "/manage/template/addTemplateItem",
  "parentId": "jok29F8KMye",
  
  "icon": "",
  
  "comments": "新增模板单元",
  "date": "2019-09-29T07:04:57.654Z",
  "v": 0
},
{
  "id": "MLP8QxL6u",
  "label": "adminLogOut",
  "type": "1",
  "apiPath": "/manage/logout",
  "parentId": "r1fTSk65-",
  
  "icon": "",
  
  "comments": "退出登录",
  "date": "2019-07-24T16:26:09.728Z",
  "sort": 2,
  "enable": true,
  "v": 0
},
{
  "id": "MKqeoofX",
  "isExt": true,
  "enable": true,
  "sort": 0,
  "label": "contentMessageManage",
  "type": "0",
  
  "parentId": "SkFHdYElb",
  "apiPath": "/admin/contentMessage",
  "routePath": "contentMessage",

  "icon": "icon_service",
  "componentPath": "contentMessage/index",
  "comments": "文档留言",
  "date": "2020-03-09T10:55:39.512Z",
  "v": 0
},
{
  "id": "LlI_t5Pa",
  "isExt": true,
  "enable": true,
  "sort": 22,
  "label": "siteMessageManage",
  "type": "0",
  
  "parentId": "SkFHdYElb",
  "apiPath": "/admin/siteMessage",
  "routePath": "siteMessage",

  "icon": "icon_shakehands_fill",
  "componentPath": "siteMessage/index",
  "comments": "站点消息",
  "date": "2020-01-05T04:28:05.207Z",
  "v": 0
},
{
  "id": "KXLs5rzbj",
  "isExt": true,
  "enable": true,
  "sort": 4,
  "label": "adsUpdate",
  "type": "1",
  "apiPath": "/manage/ads/updateOne",
  "parentId": "vbqzEsl_",
  
  "icon": "",
  
  "comments": "更新广告信息",
  "date": "2020-02-19T03:45:34.847Z",
  "v": 0
},
{
  "id": "KJ7OHV7Pf76",
  "isExt": true,
  "enable": true,
  "sort": 1,
  "label": "regUserList",
  "type": "1",
  "apiPath": "/manage/regUser/getList",
  "parentId": "3BJ23iVRM3r",
  
  "icon": "",
  
  "comments": "获取会员列表",
  "date": "2019-09-29T07:04:57.581Z",
  "v": 0
},
{
  "id": "Jp_o65bZo",
  "isExt": true,
  "enable": true,
  "sort": 2,
  "label": "announceCreate",
  "type": "1",
  "apiPath": "/manage/systemAnnounce/addOne",
  "parentId": "wnIQRCGp",
  
  "icon": "",
  
  "comments": "新增公告",
  "date": "2020-01-10T02:27:17.239Z",
  "v": 0
},
{
  "id": "Jg6GoxDB",
  "isExt": false,
  "enable": true,
  "sort": 3,
  "label": "",
  "type": "1",
  "apiPath": "/manage/mailTemplate/addOne",
  "parentId": "fD--BHfJ",
  
  "icon": "",
  
  "comments": "新增邮件模板",
  "date": "2020-02-13T12:32:43.085Z",
  "v": 0
},
{
  "id": "Jbm8t44z",
  "isExt": false,
  "enable": true,
  "sort": 1,
  "label": "",
  "type": "1",
  "apiPath": "/manage/mailTemplate/getList",
  "parentId": "fD--BHfJ",
  
  "icon": "",
  
  "comments": "获取邮件模板",
  "date": "2020-02-13T12:32:00.363Z",
  "v": 0
},
{
  "id": "JQ1F8t5me",
  "isExt": false,
  "enable": true,
  "sort": 6,
  "label": "",
  "type": "1",
  "apiPath": "/manage/adminResource/updateParentId",
  "parentId": "rkI6YtEgW",
  
  "icon": "",
  
  "comments": "更新资源父级目录",
  "date": "2019-09-29T06:49:22.476Z",
  "v": 0
},
{
  "id": "J9lxuRVWE",
  "isExt": true,
  "enable": true,
  "sort": 2,
  "label": "contentGetOne",
  "type": "1",
  "apiPath": "/manage/content/getContent",
  "parentId": "_KLwaD8s",
  
  "icon": "",
  
  "comments": "获取单条文档信息",
  "date": "2019-12-18T08:13:24.508Z",
  "v": 0
},
{
  "id": "IJIzUZpoCC-",
  "isExt": true,
  "enable": true,
  "sort": 5,
  "label": "contentTagsRemoves",
  "type": "1",
  "apiPath": "/manage/contentTag/deleteTag",
  "parentId": "9hvFygZHo_F",
  
  "icon": "",
  
  "comments": "删除标签",
  "date": "2019-09-29T07:04:57.537Z",
  "v": 0
},
{
  "id": "IEzE2AaD",
  "isExt": false,
  "enable": true,
  "sort": 2,
  "label": "",
  "type": "1",
  "apiPath": "/manage/contentMessage/getOne",
  "parentId": "1Nt0WrJ1",
  
  "icon": "",
  
  "comments": "获取单条留言信息",
  "date": "2020-03-04T07:53:06.815Z",
  "v": 0
},
{
  "id": "Hyg52FEgZ",
  "v": 0,
  "apiPath": "/manage/adminGroup/updateOne",
  "comments": "更新角色信息",
  "date": "2017-05-13T13:39:20.425Z",
  "label": "updateRoles",
  "parentId": "BJmnKKVlZ",
  "sort": 3,
  "type": "1",
  "enable": true,
  
  "icon": "",
  "apiPath": "/admin/"
},
{
  "id": "HkHTzKcN-",
  "label": "sysTermConfig",
  "type": "0",
  
  "parentId": "SkFHdYElb",
  "apiPath": "/admin/systemConfig",
  "routePath": "systemConfig",

  "componentPath": "systemConfig/index",
  "comments": "系统配置",
  "date": "2017-07-05T15:16:13.202Z",
  "sort": 4,
  "enable": true,
  "v": 0,
  "icon": "icon_setting"
},
{
  "id": "Hjwd5sYIU",
  "isExt": true,
  "enable": true,
  "sort": 7,
  "label": "contentRoofPlacement",
  "type": "1",
  "apiPath": "/manage/content/roofContent",
  "parentId": "_KLwaD8s",
  
  "icon": "",
  
  "comments": "文档置顶",
  "date": "2019-12-18T08:13:24.525Z",
  "v": 0
},
{
  "id": "HJVnRK4e-",
  "v": 0,
  "apiPath": "/manage/adminResource/deleteResource",
  "comments": "删除资源信息",
  "date": "2017-05-13T13:48:27.565Z",
  "label": "delResources",
  "parentId": "rkI6YtEgW",
  "sort": 5,
  "type": "1",
  "enable": true,
  
  "icon": "",
  "apiPath": "/admin/"
},
{
  "id": "H5JCjbGmV",
  "isExt": true,
  "enable": true,
  "sort": 1,
  "label": "announceList",
  "type": "1",
  "apiPath": "/manage/systemAnnounce/getList",
  "parentId": "wnIQRCGp",
  
  "icon": "",
  
  "comments": "获取系统公告列表",
  "date": "2020-01-10T02:27:17.234Z",
  "v": 0
},
{
  "id": "H1kjttNg-",
  "v": 0,
  "comments": "用户管理",
  "date": "2017-05-13T13:26:46.581Z",
  "label": "adminUser",
  "parentId": "SkFHdYElb",
  "sort": 1,
  "type": "0",
  
  "apiPath": "/admin/adminUser",
  "routePath": "adminUser",

  "componentPath": "adminUser/index",
  "enable": true,
  "icon": "icon_patriarch"
},
{
  "id": "H10KCFElW",
  "v": 0,
  "apiPath": "/manage/adminResource/updateOne",
  "comments": "编辑资源信息",
  "date": "2017-05-13T13:47:50.139Z",
  "label": "updateResources",
  "parentId": "rkI6YtEgW",
  "sort": 3,
  "type": "1",
  "enable": true,
  
  "icon": "",
  "apiPath": "/admin/"
},
{
  "id": "GQntvXCys",
  "isExt": true,
  "enable": true,
  "sort": 4,
  "label": "contentMessageRemoves",
  "type": "1",
  "apiPath": "/manage/contentMessage/deleteMessage",
  "parentId": "MKqeoofX",
  
  "icon": "",
  
  "comments": "删除留言",
  "date": "2020-03-09T10:55:39.528Z",
  "v": 0
},
{
  "id": "G5V9Tgv8",
  "isExt": false,
  "enable": true,
  "sort": 2,
  "label": "mailDelivery",
  "type": "0",
  
  "parentId": "BzVRSYXu",
  "apiPath": "/admin/mailDelivery",
  "routePath": "mailDelivery",

  "icon": "icon_email",
  
  "comments": "邮件发送",
  "date": "2020-02-13T12:31:38.621Z",
  "v": 0
},
{
  "id": "FD2f2xHOL",
  "isExt": true,
  "enable": true,
  "sort": 4,
  "label": "contentUpdate",
  "type": "1",
  "apiPath": "/manage/content/updateOne",
  "parentId": "_KLwaD8s",
  
  "icon": "",
  
  "comments": "更新文档信息",
  "date": "2019-12-18T08:13:24.516Z",
  "v": 0
},
{
  "id": "EbENuFg5dsi",
  "isExt": true,
  "enable": true,
  "sort": 4,
  "label": "regUserRemoves",
  "type": "1",
  "apiPath": "/manage/regUser/deleteUser",
  "parentId": "3BJ23iVRM3r",
  
  "icon": "",
  
  "comments": "删除会员",
  "date": "2019-09-29T07:04:57.600Z",
  "v": 0
},
{
  "id": "ECymwz1lav",
  "label": "testComponentGetList",
  "type": "1",
  "apiPath": "/manage/testComponent/getList",
  "parentId": "7YXd1TmTJ",
  "sort": 1,
  
  "icon": "",
  
  "enable": true,
  "comments": "获取模块测试",
  "date": "2019-07-25T10:02:07.552Z"
},
{
  "id": "Dz763h2ox69",
  "isExt": true,
  "enable": true,
  "sort": 3,
  "label": "contentTagsCreate",
  "type": "1",
  "apiPath": "/manage/contentTag/addOne",
  "parentId": "9hvFygZHo_F",
  
  "icon": "",
  
  "comments": "添加单个标签",
  "date": "2019-09-29T07:04:57.532Z",
  "v": 0
},
{
  "id": "DvrBHUvVL",
  "isExt": true,
  "enable": true,
  "sort": 2,
  "label": "contentMessageGetOne",
  "type": "1",
  "apiPath": "/manage/contentMessage/getOne",
  "parentId": "MKqeoofX",
  
  "icon": "",
  
  "comments": "获取单条留言信息",
  "date": "2020-03-09T10:55:39.521Z",
  "v": 0
},
{
  "id": "DCKT6YRg",
  "isExt": false,
  "enable": true,
  "sort": 11,
  "label": "",
  "type": "1",
  "apiPath": "/manage/content/moveCate",
  "parentId": "_KLwaD8s",
  
  "icon": "",
  
  "comments": "批量移动",
  "date": "2020-02-21T07:14:23.303Z",
  "v": 0
},
{
  "id": "C_yuiyNvLcc",
  "isExt": true,
  "enable": true,
  "sort": 5,
  "label": "templateConfigGetTempsFromShop",
  "type": "1",
  "apiPath": "/manage/template/getTempsFromShop",
  "parentId": "jok29F8KMye",
  
  "icon": "",
  
  "comments": "获取模板市场中的模板列表",
  "date": "2019-09-29T07:04:57.661Z",
  "v": 0
},
{
  "id": "CDyzNO0rdXY",
  "isExt": true,
  "enable": true,
  "sort": 3,
  "label": "templateConfigDelTemplateItem",
  "type": "1",
  "apiPath": "/manage/template/delTemplateItem",
  "parentId": "jok29F8KMye",
  
  "icon": "",
  
  "comments": "删除模板单元",
  "date": "2019-09-29T07:04:57.657Z",
  "v": 0
},
{
  "id": "BzVRSYXu",
  "isExt": false,
  "enable": true,
  "sort": 4,
  "label": "emailManage",
  "type": "0",
  
  "parentId": "0",
  "apiPath": "/admin/emailManage",
  "routePath": "emailManage",

  "icon": "icon_cmail",
  
  "comments": "邮件管理",
  "date": "2020-02-13T12:30:31.454Z",
  "v": 0
},
{
  "id": "BywRcKNxW",
  "v": 0,
  "apiPath": "/manage/adminUser/addOne",
  "comments": "添加新用户",
  "date": "2017-05-13T13:31:59.425Z",
  "label": "addNewAdminUser",
  "parentId": "H1kjttNg-",
  "sort": 1,
  "type": "1",
  "enable": true,
  
  "icon": "",
  "apiPath": "/admin/"
},
{
  "id": "BycritEeW",
  "v": 0,
  "apiPath": "/manage/adminUser/deleteUser",
  "comments": "删除用户功能",
  "date": "2017-05-13T13:33:53.942Z",
  "label": "delAdminUser",
  "parentId": "H1kjttNg-",
  "sort": 4,
  "type": "1",
  "enable": true,
  
  "icon": "",
  "apiPath": "/admin/"
},
{
  "id": "BkrPnF4lW",
  "v": 0,
  "apiPath": "/manage/adminGroup/addOne",
  "comments": "新增角色功能",
  "date": "2017-05-13T13:38:36.847Z",
  "label": "addRoles",
  "parentId": "BJmnKKVlZ",
  "sort": 2,
  "type": "1",
  "enable": true,
  
  "icon": "",
  "apiPath": "/admin/"
},
{
  "id": "BkMrhYEe-",
  "v": 0,
  "apiPath": "/manage/adminGroup/getList",
  "comments": "获取角色列表",
  "date": "2017-05-13T13:38:02.002Z",
  "label": "getRoleList",
  "parentId": "BJmnKKVlZ",
  "sort": 1,
  "type": "1",
  "enable": true,
  
  "icon": "",
  "apiPath": "/admin/"
},
{
  "id": "Bk3DAtNgW",
  "v": 0,
  "apiPath": "/manage/adminResource/addOne",
  "comments": "添加资源信息",
  "date": "2017-05-13T13:47:16.475Z",
  "label": "addResources",
  "parentId": "rkI6YtEgW",
  "sort": 2,
  "type": "1",
  "enable": true,
  
  "icon": "",
  "apiPath": "/admin/"
},
{
  "id": "BQfUHxF5byR",
  "isExt": true,
  "enable": true,
  "sort": 1,
  "label": "contentTempGetContentDefaultTemplate",
  "type": "1",
  "apiPath": "/manage/template/getTemplateForderList",
  "parentId": "ar1tFGyQeTY",
  
  "icon": "",
  
  "comments": "获取模板文件列表",
  "date": "2019-09-29T07:04:57.547Z",
  "v": 0
},
{
  "id": "BQ_sVHYI",
  "isExt": false,
  "enable": true,
  "sort": 1,
  "label": "",
  "type": "1",
  "apiPath": "/manage/contentMessage/getList",
  "parentId": "1Nt0WrJ1",
  
  "icon": "",
  
  "comments": "获取留言列表",
  "date": "2020-03-04T07:52:40.194Z",
  "v": 0
},
{
  "id": "BJmnKKVlZ",
  "v": 0,
  "comments": "角色管理",
  "date": "2017-05-13T13:27:07.469Z",
  "label": "roleManager",
  "parentId": "SkFHdYElb",
  "sort": 2,
  "type": "0",
  
  "apiPath": "/admin/adminGroup",
  "routePath": "adminGroup",

  "componentPath": "adminGroup/index",
  "enable": true,
  "icon": "icon_subordinate"
},
{
  "id": "BJ-cqKEe-",
  "v": 0,
  "apiPath": "/manage/adminUser/getList",
  "comments": "查询用户列表",
  "date": "2017-05-13T13:30:49.224Z",
  "label": "getAdminUserList",
  "parentId": "H1kjttNg-",
  "sort": 2,
  "type": "1",
  "enable": true,
  
  "icon": "",
  "apiPath": "/admin/"
},
{
  "id": "AehsXQ3z",
  "isExt": false,
  "enable": true,
  "sort": 2,
  "label": "",
  "type": "1",
  "apiPath": "/manage/uploadFile/updateOne",
  "parentId": "bL1dr2m2",
  
  "icon": "",
  
  "comments": "更新上传配置",
  "date": "2019-11-21T08:43:28.669Z",
  "v": 0
},
{
  "id": "9hvFygZHo_F",
  "isExt": true,
  "enable": true,
  "sort": 3,
  "label": "contentTagsManage",
  "type": "0",
  
  "parentId": "Zt6X0SMVF",
  "apiPath": "/admin/contentTags",
  "routePath": "contentTags",

  "icon": "icon_tags",
  "componentPath": "contentTags/index",
  "comments": "文档标签",
  "date": "2019-09-29T07:04:57.526Z",
  "v": 0
},
{
  "id": "6MAc8E1_V",
  "isExt": true,
  "enable": true,
  "sort": 8,
  "label": "contentRedictContentToUsers",
  "type": "1",
  "apiPath": "/manage/content/updateContentEditor",
  "parentId": "_KLwaD8s",
  
  "icon": "",
  
  "comments": "绑定编辑",
  "date": "2019-12-18T08:13:24.527Z",
  "v": 0
},
{
  "id": "5tXUFzP9",
  "isExt": false,
  "enable": true,
  "sort": 1,
  "label": "",
  "type": "1",
  "apiPath": "/manage/plugin/getList",
  "parentId": "_FgnLuIS",
  
  "icon": "",
  
  "comments": "获取插件列表",
  "date": "2019-10-25T17:01:19.703Z",
  "v": 0
},
{
  "id": "5llL0Y8W1",
  "isExt": true,
  "enable": true,
  "sort": 1,
  "label": "adsList",
  "type": "1",
  "apiPath": "/manage/ads/getList",
  "parentId": "vbqzEsl_",
  
  "icon": "",
  
  "comments": "获取广告列表",
  "date": "2020-02-19T03:45:34.827Z",
  "v": 0
},
{
  "id": "5hkbZDmYV1h",
  "isExt": true,
  "enable": true,
  "sort": 9,
  "label": "templateConfigUninstallTemp",
  "type": "1",
  "apiPath": "/manage/template/uninstallTemp",
  "parentId": "jok29F8KMye",
  
  "icon": "",
  
  "comments": "卸载模板",
  "date": "2019-09-29T07:04:57.670Z",
  "v": 0
},
{
  "id": "5aYlHoqqc",
  "isExt": true,
  "enable": true,
  "sort": 2,
  "label": "backUpDataBackUpData",
  "type": "1",
  "apiPath": "/manage/backupData/admin/backUp",
  "parentId": "m97HZylz",
  
  "icon": "",
  
  "comments": "执行数据备份",
  "date": "2020-03-11T09:41:31.345Z",
  "v": 0
},
{
  "id": "5EKmTw0_Fbb",
  "isExt": true,
  "enable": true,
  "sort": 2,
  "label": "regUserGetOne",
  "type": "1",
  "apiPath": "/manage/regUser/getOne",
  "parentId": "3BJ23iVRM3r",
  
  "icon": "",
  
  "comments": "获取单条会员信息",
  "date": "2019-09-29T07:04:57.590Z",
  "v": 0
},
{
  "id": "4Lsfjh1o4",
  "isExt": true,
  "enable": true,
  "sort": 2,
  "label": "siteMessageRemoves",
  "type": "1",
  "apiPath": "/manage/siteMessage/delete",
  "parentId": "LlI_t5Pa",
  
  "icon": "",
  
  "comments": "删除站点消息",
  "date": "2020-01-05T04:28:05.213Z",
  "v": 0
},
{
  "id": "42yBrE5Nc",
  "isExt": true,
  "enable": true,
  "sort": 2,
  "label": "helpCenterGetOne",
  "type": "1",
  "apiPath": "/manage/helpCenter/getOne",
  "parentId": "-O2jAnfo",
  
  "icon": "",
  
  "comments": "获取单条帮助信息",
  "date": "2020-02-19T04:30:49.970Z",
  "v": 0
},
{
  "id": "3BJ23iVRM3r",
  "isExt": true,
  "enable": true,
  "sort": 1,
  "label": "regUserManage",
  "type": "0",
  
  "parentId": "wKU6UC0Bp",
  "apiPath": "/admin/regUser",
  "routePath": "regUser",

  "icon": "icon_signal",
  "componentPath": "regUser/index",
  "comments": "会员管理",
  "date": "2019-09-29T07:04:57.578Z",
  "v": 0
},
{
  "id": "39QlsHv60",
  "isExt": true,
  "enable": true,
  "sort": 3,
  "label": "contentMessageCreate",
  "type": "1",
  "apiPath": "/manage/contentMessage/addOne",
  "parentId": "MKqeoofX",
  
  "icon": "",
  
  "comments": "添加单个留言",
  "date": "2020-03-09T10:55:39.524Z",
  "v": 0
},
{
  "id": "2fUZgE06",
  "isExt": false,
  "enable": true,
  "sort": 4,
  "label": "",
  "type": "1",
  "apiPath": "/manage/contentCategory/updateOne",
  "parentId": "R9gC7qTn",
  
  "icon": "",
  
  "comments": "更新单个类别",
  "date": "2020-02-21T07:38:52.941Z",
  "v": 0
},
{
  "id": "2_1cSL_Z_5S",
  "isExt": true,
  "enable": true,
  "sort": 1,
  "label": "contentTagsList",
  "type": "1",
  "apiPath": "/manage/contentTag/getList",
  "parentId": "9hvFygZHo_F",
  
  "icon": "",
  
  "comments": "获取标签列表",
  "date": "2019-09-29T07:04:57.528Z",
  "v": 0
},
{
  "id": "2IJDj3eow",
  "label": "getOneRole",
  "type": "1",
  "apiPath": "/manage/adminGroup/getOne",
  "parentId": "BJmnKKVlZ",
  
  "icon": "",
  
  "comments": "获取单个角色",
  "date": "2019-07-25T05:27:43.365Z",
  "sort": 5,
  "enable": true,
  "v": 0
},
{
  "id": "2-fR-TWSk",
  "isExt": true,
  "enable": true,
  "sort": 3,
  "label": "systemNotifyRemoves",
  "type": "1",
  "apiPath": "/manage/systemNotify/deleteNotifyItem",
  "parentId": "W1kIt2Vb",
  
  "icon": "",
  
  "comments": "删除操作日志",
  "date": "2020-02-21T05:45:31.280Z",
  "v": 0
},
{
  "id": "1vNd58tjI",
  "isExt": true,
  "enable": true,
  "sort": 1,
  "label": "helpCenterList",
  "type": "1",
  "apiPath": "/manage/helpCenter/getList",
  "parentId": "-O2jAnfo",
  
  "icon": "",
  
  "comments": "获取帮助列表",
  "date": "2020-02-19T04:30:49.966Z",
  "v": 0
},
{
  "id": "1Nt0WrJ1",
  "isExt": false,
  "enable": true,
  "sort": 4,
  "label": "contentMessage",
  "type": "0",
  
  "parentId": "Zt6X0SMVF",
  "apiPath": "/admin/contentMessage",
  "routePath": "contentMessage",

  "icon": "icon_sms",
  
  "comments": "文档留言",
  "date": "2020-03-04T07:52:12.218Z",
  "v": 0
},
{
  "id": "1EuBA3Lx4",
  "label": "getOneResource",
  "type": "1",
  "apiPath": "/manage/adminResource/getOne",
  "parentId": "rkI6YtEgW",
  
  "icon": "",
  
  "comments": "获取单个资源",
  "date": "2019-07-25T05:28:13.307Z",
  "sort": 5,
  "enable": true,
  "v": 0
},
{
  "id": "10g3jL8lF",
  "isExt": true,
  "enable": true,
  "sort": 3,
  "label": "backUpDataRemoves",
  "type": "1",
  "apiPath": "/manage/backupData/admin/deleteDataItem",
  "parentId": "m97HZylz",
  
  "icon": "",
  
  "comments": "删除备份数据",
  "date": "2020-03-11T09:41:31.346Z",
  "v": 0
},
{
  "id": "0QCOzomfQlT",
  "isExt": true,
  "enable": true,
  "sort": 3,
  "label": "contentTempUpdateFileInfo",
  "type": "1",
  "apiPath": "/manage/template/updateTemplateFileText",
  "parentId": "ar1tFGyQeTY",
  
  "icon": "",
  
  "comments": "更新文件内容",
  "date": "2019-09-29T07:04:57.552Z",
  "v": 0
},
{
  "id": "-wxeb8kgHpn",
  "isExt": true,
  "enable": true,
  "sort": 1,
  "label": "templateConfigGetMyTemplateList",
  "type": "1",
  "apiPath": "/manage/template/getMyTemplateList",
  "parentId": "jok29F8KMye",
  
  "icon": "",
  
  "comments": "获取已安装的模板列表",
  "date": "2019-09-29T07:04:57.652Z",
  "v": 0
},
{
  "id": "-OgUQh7x1",
  "label": "getUserSession",
  "type": "1",
  "apiPath": "/manage/getUserSession",
  "parentId": "r1fTSk65-",
  
  "icon": "",
  
  "comments": "获取用户session",
  "date": "2019-07-24T16:26:58.458Z",
  "sort": 3,
  "enable": true,
  "v": 0
},
{
  "id": "-O2jAnfo",
  "isExt": true,
  "enable": true,
  "sort": 12,
  "label": "helpCenterManage",
  "type": "0",
  
  "parentId": "SkFHdYElb",
  "apiPath": "/admin/helpCenter",
  "routePath": "helpCenter",

  "icon": "icon_service",
  "componentPath": "helpCenter/index",
  "comments": "帮助中心",
  "date": "2020-02-19T04:30:49.963Z",
  "v": 0
},
{
  "id": "-J8RLI5G",
  "isExt": false,
  "enable": true,
  "sort": 1,
  "label": "",
  "type": "1",
  "apiPath": "/manage/mailDelivery/getList",
  "parentId": "G5V9Tgv8",
  
  "icon": "",
  
  "comments": "获取发送列表",
  "date": "2020-02-13T12:34:15.816Z",
  "v": 0
}   ,
{
  "id":"Sj8gtDddN",
    "isExt": false,
    "enable": true,
    "sort": 999,
    "label": "",
    "type": "1",
    "apiPath": "/manage/logout",
    "parentId": "0",
    "icon": "",
    "date": "2020-02-13T12:35:32.681Z",
    "isWriteList" : true,
    "v": 0
  },

  {
    "id":"AFlcKrIm2w",
    "isExt": false,
    "enable": true,
    "sort": 999,
    "label": "",
    "type": "1",
    "apiPath": "/manage/getUserSession",
    "parentId": "0",
    "icon": "",
    "date": "2020-02-13T12:35:32.681Z",
    "isWriteList" : true,
    "v": 0
  },

  {
    "id":"3K4VmX1WIH",
    "isExt": false,
    "enable": true,
    "sort": 999,
    "label": "",
    "type": "1",
    "apiPath": "/manage/getSitBasicInfo",
    "parentId": "0",
    "icon": "",
    "date": "2020-02-13T12:35:32.681Z",
    "isWriteList" : true,
    "v": 0
  },

  {
    "id":"knC9WUjs8M",
    "isExt": false,
    "enable": true,
    "sort": 999,
    "label": "",
    "type": "1",
    "apiPath": "/manage/adminResource/getListByPower",
    "parentId": "0",
    "icon": "",
    "date": "2020-02-13T12:35:32.681Z",
    "isWriteList" : true,
    "v": 0
  },

  {
    "id":"vKbDX91xdP",
    "isExt": false,
    "enable": true,
    "sort": 999,
    "label": "",
    "type": "1",
    "apiPath": "/manage/plugin/pluginHeartBeat",
    "parentId": "0",
    "icon": "",
    "date": "2020-02-13T12:35:32.681Z",
    "isWriteList" : true,
    "v": 0
  },

  {
    "id":"27I4WBC3uu",
    "isExt": false,
    "enable": true,
    "sort": 999,
    "label": "",
    "type": "1",
    "apiPath": "/manage/plugin/getPluginShopList",
    "parentId": "0",
    "icon": "",
    "date": "2020-02-13T12:35:32.681Z",
    "isWriteList" : true,
    "v": 0
  },

  {
    "id":"8Bk-75Z3qZ",
    "isExt": false,
    "enable": true,
    "sort": 999,
    "label": "",
    "type": "1",
    "apiPath": "/manage/plugin/getOneShopPlugin",
    "parentId": "0",
    "icon": "",
    "date": "2020-02-13T12:35:32.681Z",
    "isWriteList" : true,
    "v": 0
  },

  {
    "id":"JBXQSGna5i",
    "isExt": false,
    "enable": true,
    "sort": 999,
    "label": "",
    "type": "1",
    "apiPath": "/manage/plugin/createInvoice",
    "parentId": "0",
    "icon": "",
    "date": "2020-02-13T12:35:32.681Z",
    "isWriteList" : true,
    "v": 0
  },

  {
    "id":"Eulvq6GK7R",
    "isExt": false,
    "enable": true,
    "sort": 999,
    "label": "",
    "type": "1",
    "apiPath": "/manage/plugin/checkInvoic",
    "parentId": "0",
    "icon": "",
    "date": "2020-02-13T12:35:32.681Z",
    "isWriteList" : true,
    "v": 0
  },

  {
    "id":"38SLwDh77",
    "isExt": false,
    "enable": true,
    "sort": 999,
    "label": "",
    "type": "1",
    "apiPath": "/manage/template/getTempsFromShop",
    "parentId": "0",
    "icon": "",
    "date": "2020-02-13T12:35:32.681Z",
    "isWriteList" : true,
    "v": 0
  },

  

]







