
'use strict';

const GeneralModel = require("../core/general_model_my");
var shortid = require('shortid');

class User extends GeneralModel {}


module.exports =  async (app) => {

  const { STRING, INTEGER, DATE, BOOLEAN } = app.Sequelize;
  
  let UserSchema = { id: {
                type: STRING, primaryKey: true, 
                set(value) {
                  if (!value){
                      this.setDataValue('id', shortid.generate());
                  }else{
                      this.setDataValue('id', value); 
                  }
                }
              },
              enable: {
                type: BOOLEAN,
                default: true
              }, //用户是否有效
              userName: STRING,
              password: {
                type: STRING
              },
              email: STRING,
              qq: INTEGER,
              countryCode: {
                  type: String
              }, // 手机号前国家代码
              phoneNum: STRING,
              countryCode: {
                type: STRING
              }, // 手机号前国家代码
              idNo: INTEGER,
              idType: {
                type: STRING,
                default: '1'
              }, // 证件类型 1为身份证
              comments: {
                type: STRING,
                default: ""
              },
              introduction: {
                type: STRING,
                default: ""
              }, // 个人简介
              position: STRING, // 职位
              profession: STRING, // 职业
              industry: STRING, // 行业
              experience: STRING, // 教育经历
              company: STRING, // 大学或公司
              website: STRING, // 个人站点
              logo: {
                type: STRING,
                default: "/static/upload/images/defaultlogo.png"
              },
              group: {
                type: STRING,
                default: "0"
              }, // 0 普通用户 
              province: STRING, // 所在省份
              city: STRING, // 所在城市
              birth: {
                type: DATE
              }, // 出生年月日 2018-03-21
              gender: {
                type: STRING,
                default: '0'
              }, // 性别 0男 1女
              state: {
                type: STRING,
                default: '1' // 1正常，0删除
              },
              retrieve_time: {
                type: DATE
              }, // 用户发送激活请求的时间
              loginActive: {
                type: BOOLEAN,
                default: false
              }, // 首次登录
              deviceId: STRING, // 针对游客的设备id
            };

  User.init(UserSchema, {
    // 这是其他模型参数
    sequelize : app.myModel, // 我们需要传递连接实例
    modelName: 'User' // 我们需要选择模型名称
  });

  if (process.env.NODE_ENV == 'development'){
    console.log("User.init");

    if (app.config.isInitMyDb){
      await User.sync({ alter: true });
    }
    if (seedDatas && seedDatas.length > 0){
      const amount = await User.count();
      if (amount == 0){
        await User.bulkCreate(seedDatas);
      }
    }
  }

  return User;
};

let seedDatas = [
{
  "id": "4JiWCMhzg",
  "userName": "doramart",
  "name": "生哥",
  "password": "U2FsdGVkX180CgEl1KBV7O2l5fpSK1Yg8lgLqjC/0ZU=",
  "group": "E1XjEmqA",
  "phoneNum": "17665365092",
  "email": "doramart@qq.com",
  "comments": "This is a test",
  "logo": "https://cdn.html-js.cn/cms/upload/smallimgs/img1448202744000.jpg",
  "date": "2015-11-11T12:04:58.412Z",
  "__v": 0,
  "auth": true,
  "enable": true,
  "state": "1",
  "countryCode": "86",
  "targetEditor": "41oT6sQXl"
},
{
  "id": "B1OTQRbOW",
  "userName": "doracms",
  "name": "测试用户",
  "email": "admin@html-js.cn",
  "phoneNum": "15220033662",
  "password": "U2FsdGVkX1/LgH/C/VcJHO3lMF3m195aQ8HftcVqyNo=",
  "group": "EydZWWiR",
  "comments": "测试用户信息",
  "enable": true,
  "logo": "https://cdn.html-js.cn/cms/upload/images/defaultlogo.png",
  "date": "2017-08-16T14:16:32.417Z",
  "__v": 0,
  "auth": true,
  "state": "1",
  "countryCode": "86"
},
{
  "id": "T7XcLJaCv",
  "logo": "/static/upload/images/defaultlogo.png",
  "enable": false,
  "state": "1",
  "auth": false,
  "userName": "doramart123",
  "name": "你好",
  "email": "lcj2002025133@163.com",
  "phoneNum": "123456789",
  "countryCode": "86",
  "password": "U2FsdGVkX1/gpM6StEhpoaVTT6ZfzUNJRKjIpcL7HzY=",
  "group": "yIGXaqSwo",
  "comments": "12234",
  "date": "2020-05-25T08:43:28.266Z",
  "__v": 0
},
{
  "id": "ft0bGHVwcA",
  "logo": "/static/upload/images/defaultlogo.png",
  "enable": true,
  "state": "1",
  "auth": false,
  "userName": "lcj2002513@163.com",
  "name": "lcj345",
  "email": "lcj2002513@163.com",
  "password": "U2FsdGVkX1/7tx4kSFsPr0Q9yEtu1/Mqd9LfA5ITugY=",
  "group": "yIGXaqSwo",
  "comments": "普通会员自动新增用户",
  "date": "2020-05-25T10:04:35.102Z",
  "__v": 0
}
];


/*

              despises: [{
                type: STRING,
                ref: 'Content'
              }], // 文章或帖子
              despiseMessage: [{
                type: STRING,
                ref: 'Message'
              }], // 评论

              favorites: [{
                type: STRING,
                ref: 'Content'
              }], // 收藏文章或帖子

              praiseContents: [{
                type: STRING,
                ref: 'Content'
              }], // 点赞的文章或帖子
              praiseMessages: [{
                type: STRING,
                ref: 'Message'
              }], // 点赞的评论
              // category: {
              //   type: STRING,
              //   ref: 'ContentCategory'
              // }, // 文章类别
              followers: [{
                type: STRING,
                ref: 'User'
              }], // 关注我的创作者
              watchers: [{
                type: STRING,
                ref: 'User'
              }], // 我关注的创作者

              watchTags: [{
                type: STRING,
                ref: 'ContentTag'
              }], // 我关注的标签
*/