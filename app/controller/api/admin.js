/*
 * @Author: doramart 
 * @Date: 2019-06-27 17:16:32 
 * @Last Modified by: doramart
 * @Last Modified time: 2020-03-20 08:53:05
 */
const Controller = require('egg').Controller;
const jwt = require('jsonwebtoken')
const _ = require('lodash');
var CryptoJS = require("crypto-js");
const {
    adminUserRule
} = require('@validate')

class AdminController extends Controller {


    async login() {
        const {
            ctx
        } = this;

        try {
            
            console.log(4,ctx.session.UserInfo);
            if (ctx.session.UserInfo) {
                console.log(5,ctx.session.UserInfo);
                ctx.redirect('/'); // /admin/dashboard
            } else {

                let configs = await ctx.helper.reqJsonData('systemConfig/getConfig');
                const {
                    showImgCode,
                    siteName,
                    siteDiscription,
                    siteKeywords,
                    siteAltKeywords,
                    ogTitle,
                    siteLogo
                } = configs || [];


                await ctx.render('manage/login.html', {
                    staticRootPath: this.app.config.static.prefix,
                    showImgCode,
                    siteName,
                    siteDiscription,
                    siteKeywords,
                    siteAltKeywords,
                    ogTitle,
                    siteLogo
                });
            }
        } catch (error) {
            ctx.helper.renderFail(ctx, {
                message: error
            });
        }

    }

    async loginAction() {

        const {
            ctx,
            service
        } = this;
        try {

            let fields = ctx.request.body || {};
            const systemConfigs = await ctx.service.systemConfig.list({key:'showImgCode'});
            const {
                showImgCode
            } = systemConfigs;

            let errMsg = '';
            if (showImgCode && (!fields.imageCode || fields.imageCode != ctx.session.imageCode)) {
                errMsg = ctx.__("validate_inputCorrect", [ctx.__("label_user_imageCode")])
            }

            if (errMsg) {
                throw new Error(errMsg);
            }

            const formObj = {
                email: fields.email,
            }

            ctx.validate(adminUserRule.login(ctx), Object.assign({}, formObj, {
                password: fields.password
            }))

            let user = await ctx.service.adminUser.__item(formObj, {
                attributes: ['enable', 'password', 'id', 'email', 'userName', 'logo']
            })

            if (!_.isEmpty(user)) {

                let userPsd = user.password;
                // 兼容老的加密方式
                if (userPsd !== CryptoJS.MD5(this.app.config.salt_md5_key + fields.password).toString() &&
                    fields.password != ctx.helper.decrypt(userPsd, this.app.config.encrypt_key)) {
                    throw new Error(ctx.__("validate_user_forbiden"));
                }

                if (!user.enable) {
                    throw new Error(ctx.__("validate_user_forbiden"));
                }

                let adminUserToken = jwt.sign({
                    id: user.id,
                }, this.app.config.encrypt_key, {
                    expiresIn: '30day'
                });

                ctx.cookies.set('admin_' + this.app.config.auth_cookie_name, adminUserToken, {
                    path: '/',
                    maxAge: this.app.config.adminUserMaxAge,
                    signed: true,
                    httpOnly: false
                }); //cookie 有效期30天

                // 记录登录日志
                let clientIp = ctx.header['x-forwarded-for'] || ctx.header['x-real-ip'] || ctx.request.ip;
                let loginLog = {
                    type: 'login',
                    logs: user.userName + ' login，ip:' + clientIp,
                };

                if (!_.isEmpty(ctx.service.systemOptionLog)) {
                    await ctx.service.systemOptionLog.create(loginLog);
                }

                ctx.helper.renderSuccess(ctx, {
                    data: {
                        token: adminUserToken
                    }
                });

            } else {
                ctx.helper.renderFail(ctx, {
                    message: ctx.__("validate_login_notSuccess")
                });
            }

        } catch (err) {
            // console.log('--err--', err)
            ctx.helper.renderFail(ctx, {
                message: err
            });

        }
    }
}

module.exports = AdminController;