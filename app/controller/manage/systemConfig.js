
const { Op } = require('sequelize');
/*
 * @Author: doramart 
 * @Date: 2019-06-20 18:55:40 
 * @Last Modified by: doramart
 * @Last Modified time: 2020-03-20 12:09:29
 */
const Controller = require('egg').Controller;

const {
    systemConfigRule
} = require('@validate')

const _ = require('lodash');


class SystemConfigController extends Controller {
    async list() {
        const {
            ctx,
            service
        } = this;
        try {

            let payload = ctx.query;
            let systemConfigList = await ctx.service.systemConfig.list({key: {[Op.not]: ['siteEmailPwd'] }});
            ctx.helper.renderSuccess(ctx, {
                data: systemConfigList
            });

        } catch (err) {

            ctx.helper.renderFail(ctx, {
                message: err
            });

        }
    }

    async update() {

        const {
            ctx,
            service
        } = this;
        try {

            let fields = ctx.request.body || {};
            const formObj = {
                siteName: fields.siteName,
                siteLogo: fields.siteLogo,
                ogTitle: fields.ogTitle,
                siteDomain: fields.siteDomain,
                siteDiscription: fields.siteDiscription,
                siteKeywords: fields.siteKeywords,
                siteAltKeywords: fields.siteAltKeywords,
                siteEmailServer: fields.siteEmailServer,
                siteEmail: fields.siteEmail,
                registrationNo: fields.registrationNo,
                databackForderPath: fields.databackForderPath,
                mongodbInstallPath: fields.mongodbInstallPath,
                showImgCode: fields.showImgCode,
                statisticalCode: fields.statisticalCode,
                bakDatabyTime: fields.bakDatabyTime,
                bakDataRate: fields.bakDataRate
            }


            ctx.validate(systemConfigRule.form(ctx), formObj);

            // 单独判断密码
            if (fields.siteEmailPwd) {
                if (fields.siteEmailPwd.length < 6) {
                    errInfo = ctx.__("validate_inputCorrect", [ctx.__("label_password")])
                } else {
                    formObj.siteEmailPwd = fields.siteEmailPwd;
                }
            }


            await ctx.service.systemConfig.UpdateOrCreate(formObj);

            ctx.helper.renderSuccess(ctx);

        } catch (err) {

            ctx.helper.renderFail(ctx, {
                message: err
            });

        }

    }


    cancelBakDataTask() {
        if (!_.isEmpty(global.bakDataTask)) {
            global.bakDataTask.cancel();
        }
    }

}

module.exports = SystemConfigController;