/*
 * @Author: doramart 
 * @Date: 2019-08-16 14:51:46 
 * @Last Modified by: doramart
 * @Last Modified time: 2020-02-17 17:36:57
 */

const {
    authToken
} = require('@utils');
const _ = require('lodash')
module.exports = (options, app) => {

    const routeWhiteList = [
        '/admin/login',
        '/dr-admin',
    ]

    return async function authAdminToken(ctx, next) {

        if (ctx.session.UserInfo){
            await next();
            return;
        }else if (!_.isEmpty(routeWhiteList)){

            let checkWhiteRouter = _.filter(routeWhiteList, (item) => {
                return ctx.originalUrl.indexOf(item) >= 0;
            })
            if (!_.isEmpty(checkWhiteRouter)) {
                await next();
                return;
            }
        }
        // 失败，重新登录
        ctx.redirect('/admin/login');
    }

}