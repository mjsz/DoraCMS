/*
 * @Author: doramart 
 * @Date: 2019-08-16 14:51:46 
 * @Last Modified by: doramart
 * @Last Modified time: 2020-03-29 17:04:40
 */
const _ = require('lodash')
module.exports = (options, app) => {

    let routeWhiteList = [];

    return async function authAdminPower(ctx, next) {

        // 添加插件中的白名单
        //if (_.isEmpty(routeWhiteList)){
        //    routeWhiteList = [
        //        /(\/manage\/)?logout/,
        //        /(\/manage\/)?getUserSession/,
        //        /(\/manage\/)?getSitBasicInfo/,
        //        /(\/manage\/)?adminResource\/getListByPower/,
        //        /(\/manage\/)?plugin\/pluginHeartBeat/,
        //        /(\/manage\/)?plugin\/getPluginShopList/,
        //        /(\/manage\/)?plugin\/getOneShopPlugin/,
        //        /(\/manage\/)?plugin\/createInvoice/,
        //        /(\/manage\/)?plugin\/checkInvoic/,
        //    ];
        //
        // 
        //
        //    let getPluginApiWhiteList = app.getExtendApiList();
        //    if (!_.isEmpty(getPluginApiWhiteList) && !_.isEmpty(getPluginApiWhiteList.adminApiWhiteList) && routeWhiteList.indexOf(getPluginApiWhiteList.adminApiWhiteList.join(',')) < 0) {
        //        for (const key in getPluginApiWhiteList.adminApiWhiteList) {
        //            if (getPluginApiWhiteList.adminApiWhiteList.hasOwnProperty(key)) {
        //                const element = getPluginApiWhiteList.adminApiWhiteList[key];
        //                routeWhiteList.push(new RegExp(`(\/manage\/)?${element}`));
        //            }
        //        }
        //    }
        //}

        let hasPower = false;
        let targetApi = ctx.originalUrl.split("?")[0];

        let resouce = await ctx.service.adminResource.find({
            where: {
                type: '1',
                apiPath : targetApi
            },
            attributes: ["id","apiPath"]
        });


        if (!_.isEmpty(ctx.session.UserInfo)) {

            hasPower = await ctx.service.adminResource.CheckPower(ctx.session.UserInfo.id, targetApi);
            //let adminPower = await ctx.helper.getAdminPower(ctx);
            //
            //for (let i = 0; i < resouce.length; i++) {
            //    let resourceObj = resouce[i];
            //    ctx.state.res_id = resourceObj._id;
            //
            //    if (resourceObj.apiPath === targetApi && adminPower && adminPower.indexOf(resourceObj.id) > -1) {
            //        hasPower = true;
            //        break;
            //    }
            //}
        }

            if (!hasPower){
                hasPower = await ctx.service.adminResource.CheckInWriteList(targetApi);
                // 没有配置但是包含在白名单内的路由校验
                //if (!_.isEmpty(routeWhiteList)) {
                //    let checkWhiteRouter = _.filter(routeWhiteList, (item) => {
                //
                //        return item.test(targetApi);
                //    })
                //    if (!_.isEmpty(checkWhiteRouter)) {
                //        hasPower = true;
                //    }
                //}
            }
        

        if (!hasPower) {
            ctx.helper.renderFail(ctx, {
                message: ctx.__('label_systemnotice_nopower')
            });
        } else {
            // console.log('check power success!')
            await next();
        }

    }

}