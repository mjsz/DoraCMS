/**
 * Created by Administrator on 2015/5/30.
 */


const _ = require('lodash');
const moment = require('moment');
const fs = require('fs');



var __ = {

    // 会改变原来Array中的Obj,会加一个children字段, 返回的是一个新的字符串
    ArrayToTree(arrOfObj, sSymbol = 'id', pSymbol = 'pid', childSymbol = 'children'){
        // 1. 两重循环，将子节点挂载到父节点中
        // 2. 将顶级节点合并成一个新的Arr返回
        let retArr = [];
        for (const i in arrOfObj) {
            const obj = arrOfObj[i];
            if (_.isObject(obj)){
                const id = obj[sSymbol];
                const pid = obj[pSymbol];
                if (!_.isUndefined(id)){
                    if (_.isUndefined(pid) || pid == id){
                        retArr.push(obj);
                    }else{
                        let bFoundParent = false;
                        for (const j in arrOfObj) {
                            const objofP = arrOfObj[j];
                            if (_.isObject(objofP) ){
                                const findid = objofP[sSymbol];
                                if (!_.isUndefined(findid) && findid == pid){
                                    bFoundParent = true;
                                    if (!_.isArray(objofP[childSymbol])){
                                        objofP[childSymbol] = [];
                                    }
                                    objofP[childSymbol].push(obj);
                                }
                            }
                        }
                        if (!bFoundParent){
                            retArr.push(obj);
                        }
                    }
                }
            }
                 
        }
        return retArr;
    }
};
module.exports = __;