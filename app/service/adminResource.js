
const {  Op } = require('sequelize');
/*
 * @Author: doramart 
 * @Date: 2019-06-24 13:20:49 
 * @Last Modified by: doramart
 * @Last Modified time: 2019-08-14 16:52:11
 */

'use strict';
const Service = require('../core/service-base');

const _ = require('lodash')
const {
    _list,
    _item,
    _count,
    _create,
    _update,
    _removes,
    _safeDelete
} = require('./general');


class AdminResourceService extends Service {

    constructor(...args){
        super(...args);
        this.Model = this.app.myModel.models.AdminResource;
    }
    async find(payload, {
        query = {},
        searchKeys = [],
        populate = [],
        files = null
    } = {}) {

        let listdata = _list(this.ctx.model.AdminResource, payload, {
            query: query,
            searchKeys: searchKeys,
            populate: populate,
            files,
            sort: {
                sortId: 1
            }
        });
        return listdata;

    }


    async count(params = {}) {
        return _count(this.ctx.model.AdminResource, params);
    }

    async create(payload) {
        return _create(this.ctx.model.AdminResource, payload);
    }

    async removes(res, values, key = '_id') {
        return _removes(res, this.ctx.model.AdminResource, values, key);
    }

    async safeDelete(res, values) {
        return _safeDelete(res, this.ctx.model.AdminResource, values);
    }

    async update(res, _id, payload) {
        return _update(res, this.ctx.model.AdminResource, _id, payload);
    }

    async item(res, params = {}) {
        return _item(res, this.ctx.model.AdminResource, params)
    }


    // 新的功能
    async GetPowerList(Userid){
        let PowerList = await this.__list({
            enable : true,
            isWriteList : false
        },{
            include: [{
                model: this.app.myModel.models.Group,
                attributes : [],
                include: [{
                    model: this.app.myModel.models.User,
                    where:{
                        id: Userid
                    },
                    attributes : [],
                }],
                where :{
                    id : {[Op.ne]: null}
                },
            }],
            orderMod : 'sort'
        });

        return PowerList;
    }

    async GetMenuPowerList(Userid){
        let PowerList = await this.__list({
            type : '0',
            enable : true,
            isWriteList : false
        },{
            include: [{
                model: this.app.myModel.models.Group,
                attributes : [],
                include: [{
                    model: this.app.myModel.models.User,
                    where:{
                        id: Userid
                    },
                    attributes : [],
                }],
                where :{
                    id : {[Op.ne]: null}
                },
            }],
            orderMod : 'sort'
        });
        return PowerList;
    }

    async GetOperatePowerList(Userid){
        let PowerList = await this.__list({
            type : '1',
            enable : true,
            isWriteList : false
        },{
            include: [{
                model: this.app.myModel.models.Group,
                attributes : [],
                include: [{
                    model: this.app.myModel.models.User,
                    where:{
                        id: Userid
                    },
                    attributes : [],
                }],
                where :{
                    id : {[Op.ne]: null}
                },
            }],
            orderMod : 'sort'
        });
        return PowerList;
    }

    async GetWriteList(){
        let wrietList = await this.__list({
            enable : true,
            isWriteList : true
        },{
            orderMod : 'sort'
        });

        return wrietList;
    }
    
    async GetWriteListOfMenu(){
        
        let wrietList = await this.__list({
            type : '0',
            enable : true,
            isWriteList : true
        },{
            orderMod : 'sort'
        });
        return wrietList;
    }
    
    async GetWriteListOfOperate(){
        
        let wrietList = await this.__list({
            type : '1',
            enable : true,
            isWriteList : true
        },{
            orderMod : 'sort'
        });
        return wrietList;
    }

    
    async CheckInWriteList(ckPath){
        
        let wrietList = await this.GetWriteList();
        let writeList = wrietList.map(i=>{
            return i.apiPath
        })
        return writeList.includes(ckPath);
    }

    
    async CheckPower(Userid, ckPath){
        let PowerCnt = await this.__count({
            enable : true,
            isWriteList : false,
            apiPath : ckPath,
        },{
            include: [{
                model: this.app.myModel.models.Group,
                attributes : [],
                include: [{
                    model: this.app.myModel.models.User,
                    where:{
                        id: Userid
                    },
                    attributes : [],
                }],
                where :{
                    id : {[Op.ne]: null}
                },
            }]
        });

        return PowerCnt > 0;
    }

}

module.exports = AdminResourceService;