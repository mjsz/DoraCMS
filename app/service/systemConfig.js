/*
 * @Author: doramart 
 * @Date: 2019-06-24 13:20:49 
 * @Last Modified by: doramart
 * @Last Modified time: 2019-08-17 21:01:44
 */


'use strict';

const Service = require('../core/service-base');

const _ = require('lodash')


class SystemConfigService extends Service {
    constructor(...args){
        super(...args);
        this.Model = this.app.myModel.models.WebConfig;
    }
    async list(where = {}) {
        
        let listdata = await super.__list(where);

        // Array to Map
        let obj = {};
        for (var item in listdata){
            obj[listdata[item].key] = listdata[item].value;
        };
        return obj;

    }

    async blukUpdateOrCreate(arrItems){
        return await this.Model.bulkCreate(arrItems,{updateOnDuplicate:["value"]});
    }

    async UpdateOrCreate(objItems){
        let arrItem = [];
        for (const key in objItems) {
            const value = objItems[key];
            arrItem.push({key,value});
        }
        return this.blukUpdateOrCreate(arrItem);
    }
}

module.exports = SystemConfigService;