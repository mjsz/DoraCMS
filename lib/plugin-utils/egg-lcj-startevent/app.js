'use strict';
const Event = require('events');


const thisevent = new Event();

class startEvent {


  constructor(app) {
    
    this.app = app;
    app.addSingleton('startevent', createOneClient);
  }

  beforeStart() {

  }

  configWillLoad() {

  }

  async didLoad() {
    thisevent.emit('come');
  }

  async willReady() {

  }
}

const cb = {
  on : function(callback){
    thisevent.on('come', callback);
  }
};

function createOneClient(config, app) {
  if (app == null){
    thisevent.on('come', config);
  }
  return cb;
}

module.exports = startEvent;

