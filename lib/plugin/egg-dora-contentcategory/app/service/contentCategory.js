/*
 * @Author: doramart 
 * @Date: 2019-06-24 13:20:49 
 * @Last Modified by: doramart
 * @Last Modified time: 2019-10-09 17:14:14
 */

'use strict';
const Service = require('egg').Service;
const path = require('path')
const _ = require('lodash')

// general是一个公共库，可用可不用
const {
    _list,
    _item,
    _count,
    _create,
    _update,
    _updateMany,
    _removes,
    _safeDelete
} = require(path.join(process.cwd(), 'app/service/general'));


class ContentCategoryService extends Service {

    async find(payload, {
        query = {},
        searchKeys = [],
        populate = [],
        files = null
    } = {}) {

        let listdata = _list(this.ctx.model.ContentCategory, payload, {
            query: query,
            searchKeys: searchKeys,
            populate: populate,
            files,
            sort: {
                sortId: 1
            }
        });
        return listdata;

    }


    async count(params = {}) {
        return _count(this.ctx.model.ContentCategory, params);
    }

    async create(payload) {
        return _create(this.ctx.model.ContentCategory, payload);
    }

    async removes(res, values, key = '_id') {
        return _removes(res, this.ctx.model.ContentCategory, values, key);
    }


    async safeDelete(res, values) {
        return _safeDelete(res, this.ctx.model.ContentCategory, values);
    }

    async update(res, _id, payload) {
        return _update(res, this.ctx.model.ContentCategory, _id, payload);
    }

    async updateMany(res, ids, payload, params) {
        return _updateMany(res, this.ctx.model.ContentCategory, ids, payload, params);
    }

    
    async star(res, _id, payload) {
        return _update(res, this.ctx.model.ContentCategory, _id, payload);
    }

    async item(res, {
        query = {},
        populate = [],
        files = null
    } = {}) {
        return _item(res, this.ctx.model.ContentCategory, {
            files: files,
            query: query,
            populate: !_.isEmpty(populate) ? populate : ['contentTemp']
        })
    }


    async catepower(res, {
        query = {},
        populate = [],
        files = null
    } = {}) {
        return _list(this.ctx.model.ContentCatePower, {isPaging:'0'},{
            files, query, populate,
        })
    }



    /**
     * @description: 判断是否有cate权限
     * @param 
     * @return: 是否有权限
     */
    async catepowerCheck(res, {
        categories = null,
        adminuser = null,
        power = null
    } = {}) {
        
      let adminUserInfo = await this.ctx.service.adminUser.item(ctx, {
            query: {
                _id: this.ctx.session.UserInfo._id,
            },
            populate: [{
                path: 'group',
                select: 'code'
            }],
            files: 'group'
        })
        if (!adminUserInfo){
            return false;
        }
        if (adminUserInfo.group.code !== 'member'){
            return true;
        }

        let it = await _item(res, this.ctx.model.ContentCatePower, {
            categories, adminuser
        });

        if (it.power == null){
            let groups = it.groups;
            let groupIt = await _list(this.ctx.model.AdminGroup, {isPaging:'0'},{
                query:{
                    _id :{$in : groups}
                }
            });

            let ArrPower = [];
            groupIt.forEach(e => {
                ArrPower.push(... e.power);
            });

            it.power = [... new Set(ArrPower)];

            await _update(res, this.ctx.model.ContentCatePower, it._id, it);
        }

        if (it.power.indexOf(power)){
            return true;
        }else{
            return false;
        }
    }
    

    

    /**
     * @description: 判断是否有cate权限
     * @param 
     * @return: 是否有权限
     */
    async catepowerCheckByContent(res, {
        contentid = null,
        adminuser = null,
        power = null
    } = {}) {

        `
        db.contents.aggregate([{
            $lookup: {
                   from: "contentcatepowers",
                   localField: "categories",
                   foreignField: "categories",
                   as: "cate"
                 }
            
        },{ $match : { _id:'0GFRtFGb' } },]
          );

          cate[0] 就是权限
        `
        
        let itContent = await _item(res, this.ctx.model.Content, {
            query:{
                _id : contentid
            }
        });

        let categories;
        if (itContent && itContent.categories && itContent.categories[0]){
            categories = itContent.categories[0];
        }else{
            return false;
        }


        return this.catepowerCheck(res, {categories,adminuser,power});
    }
    

}

module.exports = ContentCategoryService;