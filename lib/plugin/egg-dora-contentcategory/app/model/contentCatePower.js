/*
 * @Author: liucj
 * @Date: 2020-05-26 09:54:56
 * @LastEditTime: 2020-05-26 15:13:26
 * @LastEditors: Please set LastEditors
 * @Description: 用户对某一个类别的权限
 * @FilePath: \DoraCMS\lib\plugin\egg-dora-ContentCatePower\app\model\contentCatePower.js
 */ 

module.exports = app => {
  const mongoose = app.mongoose
  var shortid = require('shortid');
  var Schema = mongoose.Schema;
  var moment = require('moment')

  var ContentCatePowerSchema = new Schema({
      _id: {
          type: String,
          'default': shortid.generate
      },
      rootcategories: {
          type: String,
          ref: 'ContentCategory'
      }, //文章类别
      categories: {
          type: String,
          ref: 'ContentCategory'
      }, //文章类别
      
      adminuser: {
        type: String,
        ref: 'AdminUser'
      }, // 文档作者

      groups: [{
        type: String,
        ref: 'AdminGroup'
      }], // 权限组
      power :[{
          type: String,
          ref: "AdminResource"
      }], //所拥有的资源，通过权限组计算[...new Set(arr)]，缓存下来 
      
      date: {
          type: Date,
          default: Date.now
      },
  });

  ContentCatePowerSchema.index({
      creator: 1
  }); // 添加索引


  ContentCatePowerSchema.set('toJSON', {
      getters: true,
      virtuals: true
  });
  ContentCatePowerSchema.set('toObject', {
      getters: true,
      virtuals: true
  });

  ContentCatePowerSchema.path('date').get(function (v) {
      return moment(v).format("YYYY-MM-DD HH:mm:ss");
  });


  return mongoose.model("ContentCatePower", ContentCatePowerSchema, 'contentcatepowers');

}