/*
 * @Author: your name
 * @Date: 2020-05-26 14:52:38
 * @LastEditTime: 2020-06-24 16:04:46
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \DoraCMS\lib\plugin\egg-dora-content\app\extend\helper.js
 */ 


 /*
 * @Author: doramart 
 * @Date: 2019-08-15 14:23:19 
 * @Last Modified by: doramart
 * @Last Modified time: 2020-04-02 17:04:44
 */

require('module-alias/register')

//文件操作对象
let fs = require('fs');
let stat = fs.stat;
//TODO 老版本暂时保留，下个版本移除
var CryptoJS = require("crypto-js");
//站点配置
const validator = require('validator');
let iconv = require('iconv-lite');
const Axios = require("axios");
const _ = require('lodash');



module.exports = {

    async getAdminCatePower(ctx) {
      // todo 用户权限相关
      let adminUserInfo = await ctx.service.adminUser.__itemById(ctx.session.UserInfo.id, {
          populate: [{
              path: 'group',
              select: 'code'
          }],
          files: 'group'
      })
      if (adminUserInfo.group.code !== 'member'){
        return [true];
      }

      let catePowers = await ctx.service.contentCategory.catepower(ctx, {
          query: {
            adminuser: ctx.session.UserInfo.id,
          },
      })
      let adminCatePower = catePowers || [];
      return [false,adminCatePower];
  },
  
  // todo, 判断cate权限
  async checkAdminCatePower(ctx, param = {}) {
      return await ctx.service.contentCategory.catepowerCheck(ctx, param);
  },
};
